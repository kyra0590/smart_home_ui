import { CButton, CSpinner } from "@coreui/react";

function CLoadingButton(props) {
  const isLoading = props.isLoading ?? false;
  const disabled = props.disabled ?? false;
  return (
    <CButton
      className={props.className}
      size={props.size}
      disabled={isLoading || disabled}
      color={isLoading ? "primary" : props.color}
      onClick={props.onClick}
      style={props.style ?? { color: "#ffffff" }}
    >
      {isLoading && (
        <CSpinner
          component="span"
          size={props.size ?? "sm"}
          aria-hidden={props.aria ?? true}
          variant={props.variant ?? "border"}
        />
      )}
      {isLoading
        ? props.labelLoading ?? "Loading..."
        : props.labelButton ?? "Button"}
    </CButton>
  );
}

export default CLoadingButton;
