import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import AppStore from "src/fetch/AppFetch";
// COMPONENTS
const {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CContainer,
  CRow,
  CCol,
  CFormInput,
  CFormSelect,
  CFormLabel,
} = require("@coreui/react");

export function VoiceDialog(props) {
  const { voices, speak, title, visible, onClose, onRefresh } = props;
  const [voiceQuestion, setVoiceQuestion] = useState(0);
  const [voiceAnswer, setVoiceAnswer] = useState(0);
  const [content, setContent] = useState(
    "Hello, excited,  bread, communication"
  );

  const handleVoice = (voice) => {
    if (voices) {
      if (voice) {
        speak({ text: content, voice: voices[voice] });
      } else {
        speak({ text: content });
      }
    }
  };

  const updateDefautVoices = ({ voiceQuestion, voiceAnswer }) => {
    AppStore.storeVoices({
      voiceQuestion: voiceQuestion,
      voiceAnswer: voiceAnswer,
    });
  };

  useEffect(() => {
    const defaultVoices = AppStore.fetchVoices();
    setVoiceQuestion(defaultVoices?.voiceQuestion);
    setVoiceAnswer(defaultVoices?.voiceAnswer);
  }, []);

  const onSave = (voiceQuestion, voiceAnswer) => {
    const defaultVoices = {
      voiceQuestion: voiceQuestion,
      voiceAnswer: voiceAnswer,
    };
    updateDefautVoices(defaultVoices);
    if (onRefresh) onRefresh();
    if (onClose) onClose();
  };

  return (
    <CModal
      alignment="center"
      visible={visible}
      onClose={() => {
        if (onClose) onClose();
      }}
    >
      <CModalHeader>
        <CModalTitle>{title}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CContainer className="clearfix">
          <CRow className="mb-3">
            <CCol xs={12}>
              <CFormLabel>Voice (Question)</CFormLabel>
              <CFormSelect
                value={voiceQuestion}
                onChange={(event) => {
                  setVoiceQuestion(event?.target?.value);
                }}
              >
                <option>Selection...</option>
                {voices &&
                  voices.map((voice, index) => (
                    <option key={index} value={index}>
                      {voice.name} - {voice?.lang}
                    </option>
                  ))}
              </CFormSelect>
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xs={12}>
              <CFormLabel>Voice (Answer)</CFormLabel>
              <CFormSelect
                value={voiceAnswer}
                onChange={(event) => {
                  setVoiceAnswer(event?.target?.value);
                }}
              >
                <option>Selection...</option>
                {voices &&
                  voices.map((voice, index) => (
                    <option key={index} value={index}>
                      {voice.name} - {voice?.lang}
                    </option>
                  ))}
              </CFormSelect>
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xs={12}>
              <CFormLabel>Voice (Answer)</CFormLabel>

              <CFormInput
                placeholder="Content demo Voice"
                value={content}
                onChange={(event) => setContent(event?.target?.value)}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xs={12}>
              <CButton
                color="success"
                onClick={() => {
                  handleVoice(voiceQuestion);
                }}
                className="me-2"
              >
                Voice (Question)
              </CButton>

              <CButton
                color="warning"
                onClick={() => {
                  handleVoice(voiceAnswer);
                }}
              >
                Voice (Answer)
              </CButton>
            </CCol>
          </CRow>
        </CContainer>
      </CModalBody>
      <CModalFooter>
        <CButton
          color="secondary"
          onClick={() => {
            if (onClose) {
              onClose(false);
            }
          }}
        >
          Close
        </CButton>
        <CButton
          color="primary"
          onClick={() => {
            onSave(voiceQuestion, voiceAnswer);
          }}
        >
          Save changes
        </CButton>
      </CModalFooter>
    </CModal>
  );
}

VoiceDialog.propTypes = {
  onSave: PropTypes.func,
  onClose: PropTypes.func,
  visible: PropTypes.bool,
};
