import React, { Suspense, useEffect, useRef, useState } from "react";
import { Routes, BrowserRouter, Route } from "react-router-dom";
// COMPONENT
import AdminLayout from "./layout/AdminLayout";
import Login from "./views/admin/login/Login";
import Register from "./views/admin/register/Register";
// LIB
import history from "./lib/history";
// FETCH
import AppAuth from "src/AppAuth";
import NotFoundPage from "./views/user/NotFoundPage";
import UserLayout from "./layout/UserLayout";
import HomePage from "./views/user/HomePage";
import TestDemo from "./views/user/TestDemo";
import EnglishBooks from "./views/user/EnglishBooks";
import EnglishBook from "./views/user/EnglishBooks/EnglishBook";
import LoadingBar from "react-top-loading-bar";
import Lesson from "./views/user/EnglishBooks/Lesson";

const LOGGED_IN_STATE = {
  UNKNOWN: 0,
  LOGGED_IN: 1,
  LOGGED_OUT: 2,
};

function App(props) {
  const { isAdminPage } = props;
  const [loggedInState, setLoggedInState] = useState(LOGGED_IN_STATE.UNKNOWN);
  const ref = useRef(null);

  useEffect(() => {
    if (isAdminPage) {
      AppAuth.getUserInfo()
        .then((json) => {
          setLoggedInState(LOGGED_IN_STATE.LOGGED_IN);
        })
        .catch((ex) => {
          setLoggedInState(LOGGED_IN_STATE.LOGGED_OUT);
        });
    }
  }, [isAdminPage]);

  return (
    <>
      <LoadingBar progress={"50"} shadow={true} ref={ref} />
      <BrowserRouter history={history}>
        <Suspense>
          <Routes>
            <Route path="/admin/login" name="Login" element={<Login />} />
            <Route
              path="/admin/register"
              name="Register"
              element={<Register />}
            />
            <Route
              path="/"
              element={
                <UserLayout
                  parentClass="theme-1" // dark-theme primay_bg , theme-1
                  component={HomePage}
                  barLoading={ref}
                />
              }
            />
            <Route
              path="/english-books"
              element={
                <UserLayout
                  parentClass="theme-1" // dark-theme primay_bg , theme-1
                  component={EnglishBooks}
                  barLoading={ref}
                />
              }
            />
            <Route
              path="/english-book/:id"
              element={
                <UserLayout
                  parentClass="theme-1" // dark-theme primay_bg , theme-1
                  component={EnglishBook}
                  barLoading={ref}
                />
              }
            />
            <Route
              path="/lesson/:id/:english_book_id"
              element={
                <UserLayout
                  parentClass="theme-1" // dark-theme primay_bg , theme-1
                  component={Lesson}
                  barLoading={ref}
                />
              }
            />
            <Route path="/test" element={<TestDemo />} />
            {!isAdminPage && (
              <>
                <Route path="*" element={<NotFoundPage />} />
              </>
            )}
            {isAdminPage && (
              <Route
                path="*"
                name="Admin"
                element={
                  loggedInState === LOGGED_IN_STATE.LOGGED_IN ? (
                    <AdminLayout />
                  ) : loggedInState === LOGGED_IN_STATE.UNKNOWN ? (
                    <div />
                  ) : (
                    <Login />
                  )
                }
              />
            )}
          </Routes>
        </Suspense>
      </BrowserRouter>
    </>
  );
}

export default App;
