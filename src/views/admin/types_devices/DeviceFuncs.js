import PropTypes from "prop-types";
// COMPONENTS
import { cilPencil, cilTrash } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CTable,
  CTableRow,
  CTableHead,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CButton,
} from "@coreui/react";

function DeviceFuncs(props) {
  const { items, onOpenEditDeviceFuncDialog, onDeletDeviceFuncDialog } = props;

  return (
    <CTable striped>
      <CTableHead>
        <CTableRow>
          <CTableHeaderCell scope="col">#</CTableHeaderCell>
          <CTableHeaderCell scope="col">Name</CTableHeaderCell>
          <CTableHeaderCell scope="col">Function</CTableHeaderCell>
          <CTableHeaderCell scope="col">Action</CTableHeaderCell>
        </CTableRow>
      </CTableHead>
      <CTableBody>
        {items &&
          items.map((item) => (
            <CTableRow key={item.id}>
              <CTableHeaderCell scope="row">{item.id}</CTableHeaderCell>
              <CTableDataCell>{item.name}</CTableDataCell>
              <CTableDataCell>{item.action}</CTableDataCell>
              <CTableDataCell>
                <CButton
                  color="info"
                  className="btn-color-white"
                  onClick={() => {
                    onOpenEditDeviceFuncDialog(item);
                  }}
                >
                  <CIcon icon={cilPencil} title="Edit"></CIcon>
                </CButton>
                <CButton
                  color="danger"
                  className="m-3 btn-color-white"
                  onClick={() => {
                    onDeletDeviceFuncDialog(item);
                  }}
                >
                  <CIcon icon={cilTrash} title="Del"></CIcon>
                </CButton>
              </CTableDataCell>
            </CTableRow>
          ))}
      </CTableBody>
    </CTable>
  );
}

DeviceFuncs.propTypes = {
  isLoading: PropTypes.bool,
  items: PropTypes.array,
  onOpenEditDeviceFuncDialog: PropTypes.func,
  onDeletDeviceFuncDialog: PropTypes.func,
};

export default DeviceFuncs;
