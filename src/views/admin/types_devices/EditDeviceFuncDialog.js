import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
// COMPONENTS
const {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CContainer,
  CRow,
  CCol,
  CFormInput,
} = require("@coreui/react");

export function EditDeviceFuncDialog(props) {
  const { deviceFunc, deviceTypeId, visible, onClose, onSave } = props;

  const [name, setName] = useState("");
  const [action, setAction] = useState("");

  useEffect(() => {
    if (deviceFunc) {
      setName(deviceFunc?.name ?? "");
      setAction(deviceFunc?.action ?? "");
    } else {
      setName("");
      setAction("");
    }
  }, [deviceFunc]);

  return (
    <CModal alignment="center" visible={visible} onClose={() => onClose()}>
      <CModalHeader>
        <CModalTitle>{props.title}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CContainer className="clearfix">
          <CRow className="mb-3">
            <CCol xs={4}>Name</CCol>
            <CCol xs={12}>
              <CFormInput
                value={name}
                onChange={(event) => {
                  setName(event.target.value);
                }}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xs={4}>Function</CCol>
            <CCol xs={12}>
              <CFormInput
                value={action}
                onChange={(event) => {
                  setAction(event.target.value);
                }}
              />
            </CCol>
          </CRow>
        </CContainer>
      </CModalBody>
      <CModalFooter>
        <CButton
          color="secondary"
          onClick={() => {
            if (onClose) {
              onClose(false);
            }
          }}
        >
          Close
        </CButton>
        <CButton
          color="primary"
          onClick={() => {
            if (onSave) {
              const idFunc = deviceFunc ? deviceFunc.id : "";
              onSave(idFunc, {
                name: name,
                action: action,
                device_type_id: deviceTypeId,
              });
            }
          }}
        >
          Save changes
        </CButton>
      </CModalFooter>
    </CModal>
  );
}

EditDeviceFuncDialog.propTypes = {
  isLoading: PropTypes.bool,
  deviceFunc: PropTypes.object,
  deviceTypeId: PropTypes.string,
  onSave: PropTypes.func,
  onClose: PropTypes.func,
  visible: PropTypes.bool,
};
