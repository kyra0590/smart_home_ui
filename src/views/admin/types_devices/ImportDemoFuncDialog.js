import React, { useEffect, useState } from "react";
import DemoFuncFetch from "src/fetch/DemoFuncFetch";
import SimpleBar from "simplebar-react";

const {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CContainer,
  CRow,
  CCol,
  CFormSelect,
  CFormLabel,
} = require("@coreui/react");

export function ImportDemoFuncDialog(props) {
  const { onSave, onClose, typeDemoFuncs } = props;

  const [typeDemoFunc, setTypeDemoFunc] = useState(0);
  const [demoFuncs, setDemoFuncs] = useState(null);

  useEffect(() => {
    const loadDemoFuncs = async (typeDemoFunc) => {
      const criterias = {
        type_demo_func: typeDemoFunc,
      };
      await DemoFuncFetch.getAll(criterias)
        .then((json) => setDemoFuncs(json?.data))
        .catch((ex) => console.log(ex));
    };
    if (typeDemoFuncs) {
      loadDemoFuncs(typeDemoFunc);
    }
  }, [typeDemoFuncs, typeDemoFunc]);

  return (
    <CModal
      alignment="center"
      visible={props.visible}
      onClose={() => props.onClose()}
    >
      <CModalHeader>
        <CModalTitle>{props.title}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CContainer className="clearfix">
          <CRow className="mb-3">
            <CCol xs={12}>
              <CFormLabel>Area</CFormLabel>
              <CFormSelect
                value={typeDemoFunc}
                onChange={(event) => {
                  setTypeDemoFunc(event?.target?.value);
                }}
              >
                <option key={0} value={0}>
                  Selection...
                </option>
                {typeDemoFuncs &&
                  typeDemoFuncs.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.name}
                    </option>
                  ))}
              </CFormSelect>
            </CCol>
          </CRow>
          {demoFuncs && (
            <CRow className="mb-3">
              <CCol xs={12}>
                <CFormLabel>Functions</CFormLabel>
                <SimpleBar style={{ maxHeight: 300, marginBottom: 20 }}>
                  <ul className="list-group">
                    {demoFuncs.map((item) => (
                      <li
                        className="list-group-item list-group-item-info"
                        key={item.id}
                      >
                        {item.name}
                      </li>
                    ))}
                  </ul>
                </SimpleBar>
              </CCol>
            </CRow>
          )}
        </CContainer>
      </CModalBody>
      <CModalFooter>
        <CButton
          color="secondary"
          onClick={() => {
            if (onClose) {
              onClose(false);
            }
          }}
        >
          Close
        </CButton>
        <CButton
          color="primary"
          onClick={() => {
            if (onSave) {
              onSave(typeDemoFunc);
              setTypeDemoFunc(0);
            }
          }}
        >
          Import
        </CButton>
      </CModalFooter>
    </CModal>
  );
}
