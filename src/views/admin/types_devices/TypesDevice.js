import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
// COMPONENTS
import {
  CButton,
  CCol,
  CContainer,
  CPagination,
  CPaginationItem,
  CRow,
  CSpinner,
  CTable,
  CTableBody,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import { ToastContainer, toast } from "react-toastify";
import SimpleBar from "simplebar-react";
// FETCH
import TypesDeviceFetch from "src/fetch/TypesDeviceFetch";
import ListTypesDevices from "./ListTypesDevices";
import { Modal } from "src/components/AdminComponents/commons/Modal";

function TypesDevice(props) {
  let navigate = useNavigate();
  const [typesDevices, setTypesDevices] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [titleModal, setTitleModal] = useState();
  const [bodyModal, setBodyModal] = useState();
  const [isVisible, setIsVisible] = useState(false);
  const [totalPage, setTotalPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [typesdevice, setTypesDevice] = useState(null);
  const [height, setHeight] = useState(500);

  const loadTypesDevices = async (page = 1, limit = 20) => {
    setIsLoading(true);
    await TypesDeviceFetch.getAll({ limit: limit, page: page })
      .then((json) => {
        setTypesDevices(json?.data);
        setTotalPage(json?.last_page);
        setCurrentPage(json?.current_page);
      })
      .catch((ex) => {
        console.log(ex);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    loadTypesDevices();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  const nextPage = (nextPage) => {
    loadTypesDevices(nextPage);
  };

  const editTypesDevice = (id) => {
    navigate("/admin/edit-types-device/" + id);
  };

  const delTypesDevice = (item) => {
    setTitleModal("Delete " + item.name);
    setBodyModal("Are you sure ?");
    setIsVisible(true);
    setTypesDevice(item);
  };

  const onConfirmDelAction = async () => {
    if (typesdevice) {
      await TypesDeviceFetch.delTypesDevice(typesdevice.id)
        .then((json) => {
          showToast("Delete Successfully", false);
        })
        .catch((ex) => {
          showToast("Delete Failed", false);
        });
      loadTypesDevices();
    }
    setIsVisible(false);
    setTypesDevice(null);
  };

  const showPages = () => {
    let listPage = [];
    const end = currentPage + 5 > totalPage ? totalPage : currentPage + 5;
    const first = end - 5 > 0 ? end - 5 : 1;
    for (let index = first; index <= end; index++) {
      let active = index === currentPage;
      listPage.push(
        <CPaginationItem
          key={index}
          active={active}
          onClick={() => {
            console.log(index);
            nextPage(index);
          }}
        >
          {index}
        </CPaginationItem>
      );
    }
    return listPage;
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />
      <CRow className="mb-4 mr-4">
        <CCol xs="2">
          <CButton
            color="info"
            style={{ color: "#fff" }}
            onClick={() => navigate("/admin/new-types-device")}
          >
            New Type
          </CButton>
        </CCol>
        {isLoading && <CSpinner component="span" color="info" />}
      </CRow>
      <CRow>
        <CCol xs="12" mb="12" className="table-list  p-3">
          <SimpleBar style={{ padding: 20, maxHeight: height - 300 }}>
            <CTable align="middle" responsive="xxl" bordered>
              <CTableHead color="light">
                <CTableRow>
                  <CTableHeaderCell className="col-id">#</CTableHeaderCell>
                  <CTableHeaderCell>Name</CTableHeaderCell>
                  <CTableHeaderCell className="col-action">
                    Action
                  </CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                <ListTypesDevices
                  items={typesDevices}
                  isLoading={isLoading}
                  editTypesDevice={editTypesDevice}
                  delTypesDevice={delTypesDevice}
                />
              </CTableBody>
            </CTable>
          </SimpleBar>
          {!isLoading && typesDevices?.length > 0 && (
            <CPagination align="end" aria-label="Page navigation example">
              <CPaginationItem
                disabled={currentPage === 1}
                onClick={() => {
                  nextPage(currentPage === 1 ? 1 : currentPage - 1);
                }}
              >
                <span>&laquo;</span>
              </CPaginationItem>
              {showPages()}
              <CPaginationItem
                disabled={currentPage === totalPage}
                onClick={() => {
                  nextPage(currentPage + 1);
                }}
              >
                <span>&raquo;</span>
              </CPaginationItem>
            </CPagination>
          )}
        </CCol>
      </CRow>
      <Modal
        visible={isVisible}
        onClose={setIsVisible}
        title={titleModal}
        body={bodyModal}
        onSave={onConfirmDelAction}
      />
    </CContainer>
  );
}

export default TypesDevice;
