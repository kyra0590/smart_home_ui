import { cilPencil, cilTrash } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CButton,
  CTableDataCell,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import React from "react";

const ListTypesDevices = (props) => {
  return (
    <>
      {!props.isLoading &&
        props.items &&
        props.items.map((item, index) => (
          <CTableRow key={index}>
            <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
            <CTableDataCell>{item.name}</CTableDataCell>
            <CTableDataCell>
              <CButton
                color="info"
                className="btn-color-white"
                onClick={() => props.editTypesDevice(item.id)}
              >
                <CIcon icon={cilPencil} title="Edit"></CIcon>
              </CButton>
              <CButton
                color="danger"
                className="m-3 btn-color-white"
                onClick={() => props.delTypesDevice(item)}
              >
                <CIcon icon={cilTrash} title="Del"></CIcon>
              </CButton>
            </CTableDataCell>
          </CTableRow>
        ))}
    </>
  );
};

export default ListTypesDevices;
