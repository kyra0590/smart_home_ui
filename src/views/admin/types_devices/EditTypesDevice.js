import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
//FETCH
import TypesDeviceFetch from "src/fetch/TypesDeviceFetch";
import DeviceFuncFetch from "src/fetch/DeviceFuncFetch";
import TypeDemoFuncFetch from "src/fetch/TypdeDemoImportFetch";
import DemoFuncFetch from "src/fetch/DemoFuncFetch";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CForm,
  CFormInput,
  CCard,
  CFormLabel,
} from "@coreui/react";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
import { EditDeviceFuncDialog } from "./EditDeviceFuncDialog";
import { Modal } from "src/components/AdminComponents/commons/Modal";
import { ImportDemoFuncDialog } from "./ImportDemoFuncDialog";
import DeviceFuncs from "./DeviceFuncs";
import SimpleBar from "simplebar-react";
// CSS
import "react-toastify/dist/ReactToastify.css";

function EditTypesDevice() {
  // get query params
  let { id } = useParams();

  let navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const [name, setName] = useState("");
  const [typesDevice, setTypesDevice] = useState(null);
  const [deviceFuncs, setDeviceFuncs] = useState(null);
  const [titleModal, setTitleModal] = useState("title");
  const [isVisible, setIsVisible] = useState(false);
  const [deviceFunc, setDeviceFunc] = useState(null);
  const [isVisibleDelteDialog, setIsVisibleDelteDialog] = useState(false);
  const [isVisibleImportDemoDialog, setIsVisibleImportDemoDialog] =
    useState(false);
  const [typeDemoFuncs, setTypeDemoFuncs] = useState(null);
  const [height, setHeight] = useState(500);

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };
  useEffect(() => {
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  useEffect(() => {
    if (id) loadTypesDevice(id);

    const loadTypeDemoFuncs = async () => {
      await TypeDemoFuncFetch.getAll()
        .then((json) => setTypeDemoFuncs(json?.data))
        .catch((ex) => {
          console.log(ex);
        });
    };

    loadTypeDemoFuncs();
  }, [id]);

  const loadTypesDevice = async (id) => {
    await TypesDeviceFetch.getTypesDevice(id)
      .then(async (json) => {
        const typesDevice = json?.data;
        if (typesDevice) {
          setName(typesDevice?.name);
          setTypesDevice(typesDevice);
          await DeviceFuncFetch.getAll({
            device_type_id: typesDevice.id,
          })
            .then((json) => {
              setDeviceFuncs(json?.data);
            })
            .catch((ex) => {
              console.log(ex);
            });
        }
      })
      .catch((ex) => console.log(ex));
  };

  const onSaveTypesDevice = async () => {
    setIsLoading(true);
    if (!id) {
      await TypesDeviceFetch.createTypesDevice({
        name: name,
      })
        .then((json) => {
          showToast("Create Successfully", false);
          setTimeout(() => navigate("/admin/types-device"), 1000);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
        });
    } else {
      await TypesDeviceFetch.editTypesDevice(id, {
        name: name ?? "",
      })
        .then((json) => {
          showToast("Edit Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Edit Failed", true);
        });
    }
    setIsLoading(false);
  };

  // save create/edit function
  const onSaveEditDeviceFunc = async (idFunc, payload) => {
    // update
    if (idFunc) {
      await DeviceFuncFetch.editDeviceFunc(idFunc, payload)
        .then((json) => {
          loadTypesDevice(id);
          showToast("Edit Successfully", false);
        })
        .catch((ex) => {
          showToast("Edit Failed", true);
        });
    } else {
      await DeviceFuncFetch.createDeviceFunc(payload)
        .then((json) => {
          loadTypesDevice(id);
          showToast("Create Successfully", false);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
        });
    }
    setIsVisible(false);
  };

  // save delete func
  const onSaveDeleteDeviceFunc = async () => {
    await DeviceFuncFetch.delDeviceFunc(deviceFunc?.id)
      .then((json) => {
        showToast("Delete Successfully", false);
      })
      .catch((ex) => showToast("Delete failed", true));

    setIsVisibleDelteDialog(false);
    loadTypesDevice(id);
  };

  // save import demo func
  const onSaveImportDemoFunc = async (typeDemoFunc) => {
    const criterias = {
      device_type_id: typesDevice?.id,
      type_demo_func: typeDemoFunc,
    };

    await DemoFuncFetch.importDemoFuncs(criterias)
      .then((json) => {
        showToast(json?.message, false);
        loadTypesDevice(id);
      })
      .catch((ex) => showToast("Import demo functions failed", true));

    setIsVisibleImportDemoDialog(false);
  };

  // open dialog and create
  const onOpenCreateDeviceFuncDialog = () => {
    setTitleModal("Create new function");
    setIsVisible(true);
    setDeviceFunc(null);
  };

  // open dialog and edit
  const onOpenEditDeviceFuncDialog = (deviceFunc) => {
    setDeviceFunc(deviceFunc);
    setTitleModal("Edit function");
    setIsVisible(true);
  };

  // open dialog delete
  const onDeletDeviceFuncDialog = (deviceFunc) => {
    setIsVisibleDelteDialog(true);
    setDeviceFunc(deviceFunc);
  };

  // open dialog import demo functions
  const onImportDemoFuncDialog = () => {
    setIsVisibleImportDemoDialog(true);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />

      <CRow className="mb-4">
        <CCol xs="12">
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => navigate("/admin/types-device")}
          >
            Back
          </CButton>
          <CLoadingButton
            isLoading={isLoading}
            color="success"
            style={{ color: "#fff" }}
            onClick={onSaveTypesDevice}
            labelLoading="Saving..."
            labelButton="Save"
          />
        </CCol>
      </CRow>

      <SimpleBar style={{ padding: 20, maxHeight: height - 300 }}>
        <CForm className="row">
          <CCard className="p-3">
            <CRow className="mb-3">
              <CCol lg={4} xs={12}>
                <CFormLabel>Types device</CFormLabel>
                <CFormInput
                  placeholder="Types device"
                  value={name}
                  onChange={(event) => {
                    setName(event?.target?.value);
                  }}
                />
              </CCol>
            </CRow>
            {typesDevice && (
              <>
                <CRow className="mb-3">
                  <CCol lg={6} xs={12}>
                    <CFormLabel>Functions</CFormLabel>
                  </CCol>
                  <CCol lg={6} xs={12}>
                    <CButton
                      color="dark"
                      style={{ color: "#fff", marginRight: 10 }}
                      onClick={onImportDemoFuncDialog}
                    >
                      Import Sample Functions
                    </CButton>
                    <CButton
                      color="info"
                      style={{ color: "#fff", marginRight: 10 }}
                      onClick={onOpenCreateDeviceFuncDialog}
                    >
                      Create new function
                    </CButton>
                  </CCol>
                </CRow>
                <CRow className="mb-3">
                  <CCol lg={12} xs={12}>
                    <DeviceFuncs
                      items={deviceFuncs}
                      onOpenEditDeviceFuncDialog={onOpenEditDeviceFuncDialog}
                      onDeletDeviceFuncDialog={onDeletDeviceFuncDialog}
                    />
                  </CCol>
                </CRow>
              </>
            )}
          </CCard>
        </CForm>
      </SimpleBar>

      <EditDeviceFuncDialog
        visible={isVisible}
        onClose={setIsVisible}
        title={titleModal}
        deviceFunc={deviceFunc}
        onSave={onSaveEditDeviceFunc}
        deviceTypeId={id}
      />

      <Modal
        visible={isVisibleDelteDialog}
        onClose={setIsVisibleDelteDialog}
        title={deviceFunc ? "Delete " + deviceFunc.name : ""}
        body={"Are you sure ?"}
        onSave={onSaveDeleteDeviceFunc}
      />

      <ImportDemoFuncDialog
        visible={isVisibleImportDemoDialog}
        onClose={setIsVisibleImportDemoDialog}
        title={"Import Sample Functions"}
        onSave={onSaveImportDemoFunc}
        typeDemoFuncs={typeDemoFuncs}
      />
    </CContainer>
  );
}

export default EditTypesDevice;
