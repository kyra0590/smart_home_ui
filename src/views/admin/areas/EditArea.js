import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
//FETCH
import AreasFetch from "src/fetch/AreasFetch";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CForm,
  CFormInput,
  CCard,
  CFormLabel,
} from "@coreui/react";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
// CSS
import "react-toastify/dist/ReactToastify.css";

function EditArea() {
  // get query params
  let { id } = useParams();

  let navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const [name, setName] = useState("");
  const [fullName, setFullName] = useState("");

  useEffect(() => {
    const loadArea = async (id) => {
      await AreasFetch.getArea(id)
        .then((json) => {
          const area = json?.data;
          if (area) {
            setName(area?.name);
            setFullName(area?.full_name);
          }
        })
        .catch((ex) => console.log(ex));
    };

    if (id) loadArea(id);
  }, [id]);

  const onSaveArea = async () => {
    setIsLoading(true);
    if (!id) {
      await AreasFetch.createArea({
        name: name,
        full_name: fullName,
      })
        .then((json) => {
          showToast("Create Successfully", false);
          setTimeout(() => navigate("/areas"), 1000);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
          console.log(ex);
        });
    } else {
      await AreasFetch.editArea(id, {
        name: name ?? "",
        full_name: fullName ?? "",
      })
        .then((json) => {
          showToast("Edit Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Edit Failed", true);
        });
    }
    setIsLoading(false);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />

      <CRow className="mb-4">
        <CCol xs="12">
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => navigate("/admin/areas")}
          >
            Back
          </CButton>
          <CLoadingButton
            isLoading={isLoading}
            color="success"
            style={{ color: "#fff" }}
            onClick={onSaveArea}
            labelLoading="Saving..."
            labelButton="Save"
          />
        </CCol>
      </CRow>

      <CForm className="row">
        <CCard className="p-3">
          <CRow className="mb-3">
            <CCol lg={4} xs={12}>
              <CFormLabel>Area Name</CFormLabel>
              <CFormInput
                placeholder="name"
                value={name}
                onChange={(event) => {
                  setName(event?.target?.value);
                }}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol lg={4} xs={12}>
              <CFormLabel>Area Full Name</CFormLabel>
              <CFormInput
                placeholder="full name"
                value={fullName}
                onChange={(event) => {
                  setFullName(event?.target?.value);
                }}
              />
            </CCol>
          </CRow>
        </CCard>
      </CForm>
    </CContainer>
  );
}

export default EditArea;
