import React from "react";
import PropTypes from "prop-types";
// COMPONENTS
import { cilPencil, cilTrash } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CButton,
  CTableDataCell,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";

const ListAreas = (props) => {
  return (
    <>
      {!props.isLoading &&
        props.items &&
        props.items.map((item, index) => (
          <CTableRow key={index}>
            <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
            <CTableDataCell>{item.name}</CTableDataCell>
            <CTableDataCell>{item.full_name}</CTableDataCell>
            <CTableDataCell>
              <CButton
                color="info"
                className="btn-color-white"
                onClick={() => props.editArea(item.id)}
              >
                <CIcon icon={cilPencil} title="Edit"></CIcon>
              </CButton>
              <CButton
                color="danger"
                className="m-3 btn-color-white"
                onClick={() => props.delArea(item)}
              >
                <CIcon icon={cilTrash} title="Del"></CIcon>
              </CButton>
            </CTableDataCell>
          </CTableRow>
        ))}
    </>
  );
};

ListAreas.propTypes = {
  isLoading: PropTypes.bool,
  items: PropTypes.array,
  editArea: PropTypes.func,
  delArea: PropTypes.func,
};

export default ListAreas;
