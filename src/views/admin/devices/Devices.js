import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CSpinner,
  CPaginationItem,
  CPagination,
} from "@coreui/react";
import CardDevice from "src/components/AdminComponents/devices/CardDevice";
import { ToastContainer, toast } from "react-toastify";
import SimpleBar from "simplebar-react";
// FETCH
import DevicesFetch from "src/fetch/DevicesFetch";
import ReactImg from "../../../assets/images/react.jpg";
import { Modal } from "../../../components/AdminComponents/commons/Modal";
import DeviceFuncFetch from "src/fetch/DeviceFuncFetch";
import ServicesFetch from "src/fetch/ServicesFetch";

function Devices() {
  let navigate = useNavigate();
  const [devices, setDevices] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [titleModal, setTitleModal] = useState();
  const [bodyModal, setBodyModal] = useState();
  const [device, setDevice] = useState(null);
  const [totalPage, setTotalPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [height, setHeight] = useState(500);

  const loadDevices = async (page = 1, limit = 12) => {
    setIsLoading(true);
    await DevicesFetch.getAll({ limit: limit, page: page })
      .then((json) => {
        setDevices(json?.data);
        setTotalPage(json?.last_page);
        setCurrentPage(json?.current_page);
      })
      .catch((ex) => {
        console.log(ex);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const onEditAction = (device) => {
    if (device) {
      navigate(`/admin/edit-device/${device.id}`);
    }
  };

  const onDelAction = (item) => {
    setTitleModal("Delete " + item.name);
    setBodyModal("Are you sure ?");
    setIsVisible(true);
    setDevice(item);
  };

  const onConfirmDelAction = async () => {
    if (device) {
      await DevicesFetch.delDevice(device.id)
        .then((json) => {
          showToast("Delete Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Delete Failed", false);
        });
      loadDevices();
    }
    setIsVisible(false);
    setDevice(null);
  };

  const onDefaultFuncAction = async (item) => {
    await DeviceFuncFetch.getDeviceFunc(item.func_id).then(async (json) => {
      const data = json?.data;
      await testFunction(item?.ip_address_output, data?.action);
    });
  };

  const testFunction = async (ipAddress, action) => {
    console.log(ipAddress, action);
    if (ipAddress && action) {
      await ServicesFetch.testFunc({
        ip_address: ipAddress,
        action: action,
      })
        .then((json) => {
          // showToast("Test Func Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Test Func failed", true);
        });
    } else {
      showToast("Test Func failed", true);
    }
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    loadDevices();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  const nextPage = (nextPage) => {
    loadDevices(nextPage);
  };

  const showPages = () => {
    let listPage = [];
    const end = currentPage + 5 > totalPage ? totalPage : currentPage + 5;
    const first = end - 5 > 0 ? end - 5 : 1;
    for (let index = first; index <= end; index++) {
      let active = index === currentPage ? true : false;
      listPage.push(
        <CPaginationItem
          key={index}
          active={active}
          onClick={() => {
            console.log(index);
            nextPage(index);
          }}
        >
          {index}
        </CPaginationItem>
      );
    }
    return listPage;
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />
      <CRow className="mb-4">
        <CCol lg={2} xs={2}>
          <CButton
            color="info"
            style={{ color: "#fff" }}
            onClick={() => navigate("/admin/new-device")}
          >
            New Device
          </CButton>
        </CCol>
        {isLoading && <CSpinner component="span" color="info" />}
      </CRow>
      <SimpleBar style={{ padding: 20, maxHeight: height - 300 }}>
        <CRow>
          {devices &&
            devices.map((item, index) => (
              <CCol xs={12} lg={3} key={index} className="mb-4">
                <CardDevice
                  device={item}
                  ipAddress={item.ip_address}
                  title={item.name}
                  description={item.description}
                  image={ReactImg}
                  status={item.status}
                  onEditAction={() => {
                    onEditAction(item);
                  }}
                  onDelAction={() => {
                    onDelAction(item);
                  }}
                  onDefaultFuncAction={() => {
                    onDefaultFuncAction(item);
                  }}
                  showToast={showToast}
                />
              </CCol>
            ))}
        </CRow>
      </SimpleBar>
      {!isLoading && devices?.length > 0 && (
        <CPagination align="end" aria-label="Page navigation ">
          <CPaginationItem
            disabled={currentPage === 1}
            onClick={() => {
              nextPage(currentPage === 1 ? 1 : currentPage - 1);
            }}
          >
            <span>&laquo;</span>
          </CPaginationItem>
          {showPages()}
          <CPaginationItem
            disabled={currentPage === totalPage}
            onClick={() => {
              nextPage(currentPage + 1);
            }}
          >
            <span>&raquo;</span>
          </CPaginationItem>
        </CPagination>
      )}
      <Modal
        visible={isVisible}
        onClose={setIsVisible}
        title={titleModal}
        body={bodyModal}
        onSave={onConfirmDelAction}
      />
    </CContainer>
  );
}

export default Devices;
