import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
// COMPONENTS
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSwitch,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilLockLocked, cilUser } from "@coreui/icons";
import { ToastContainer, toast } from "react-toastify";
// Fetch
import AppAuth from "src/AppAuth";
import LoginFetch from "src/fetch/LoginFetch";
import { connect } from "react-redux";
import { onAppLogined } from "src/redux/actions/Actions";
// CSS
import "react-toastify/dist/ReactToastify.css";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";

const Login = (props) => {
  const [username, setUsername] = useState("smart_home@gmail.com");
  const [password, setPassword] = useState("Abcd12347");
  const [isLoading, setIsLoading] = useState(false);
  const [isRemember, setIsRemember] = useState(false);

  let navigate = useNavigate();

  useEffect(() => {
    // If user logged in on miEdge (has session already)
    const findUserInfo = () => {
      LoginFetch.findUserInfo()
        .then((json) => {
          console.log("User logged in already...");
          navigate("/admin");
        })
        .catch((ex) => {
          console.log("User not logged in already...");
          navigate("/admin/login");
        });
    };
    findUserInfo();
  }, [navigate]);

  const onChange = (e, target) => {
    const value = e.target.value;
    switch (target) {
      case "username":
        setUsername(value);
        break;
      case "password":
        setPassword(value);
        break;
      default:
        break;
    }
  };

  const onSubmit = async () => {
    setIsLoading(true);
    await AppAuth.login(username, password, isRemember)
      .then((json) => {
        if (props.reOnApplogined) {
          props.reOnApplogined(json);
        }
        showToast("Login Success", false);
        setTimeout(() => {
          window.location = "/admin";
        }, 1000);
      })
      .catch((ex) => {
        console.log(ex);
        showToast("Login Error", true);
      });
    setIsLoading(false);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <ToastContainer />
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Đăng nhập</h1>
                    <p className="text-medium-emphasis">
                      Đăng nhập tài khoản của bạn
                    </p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        placeholder="Username"
                        autoComplete="username"
                        value={username}
                        onChange={(e) => {
                          onChange(e, "username");
                        }}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        onChange={(e) => {
                          onChange(e, "password");
                        }}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CLoadingButton
                          color="info"
                          isLoading={isLoading}
                          labelButton="Login"
                          onClick={() => onSubmit()}
                          style={{ color: "#fff" }}
                        />
                      </CCol>
                      <CCol xs={6}>
                        <CFormSwitch
                          label="Remember me"
                          size="xl"
                          value={isRemember}
                          onChange={(event) => {
                            setIsRemember(event?.target?.checked);
                          }}
                        />
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard
                className="text-white bg-dark py-5"
                style={{ width: "44%" }}
              >
                <CCardBody className="text-center">
                  <div>
                    <h2>Đăng ký</h2>
                    <p>Tạo tài khoản mới</p>
                    <Link to="/register">
                      <CButton
                        color="success"
                        className="mt-3"
                        active
                        tabIndex={-1}
                        style={{ color: "#fff" }}
                      >
                        Register Now!
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  reOnApplogined: (json) => {
    dispatch(onAppLogined(json));
  },
});

export default connect(null, mapDispatchToProps)(Login);
