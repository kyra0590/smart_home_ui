import React from "react";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CFormInput,
  CForm,
} from "@coreui/react";
import { ToastContainer } from "react-toastify";
import SimpleBar from "simplebar-react";
import AvatarStatus from "./AvatarStatus";
import AnswerChat from "./AnswerChat";

function Chat() {
  return (
    <CContainer className="clearfix">
      <ToastContainer />
      <CForm className="chat">
        <CRow>
          <CCol xs="12" sm="3">
            <SimpleBar className="column-height">
              <CRow className="col-inside-lg decor-default">
                <CCol sm="12" className="pt-4 pb-4">
                  <CFormInput className="search-text" placeholder="Search" />
                </CCol>
                <div className="chat-users">
                  <h6>Online</h6>
                  <div className="users">
                    <AvatarStatus
                      status={1}
                      username="nguyen van a"
                      mood="  ..."
                    />
                  </div>
                </div>
              </CRow>
            </SimpleBar>
          </CCol>
          <CCol sm="9" xs="12">
            <SimpleBar className="column-height-2">
              <div className="col-inside-lg decor-default">
                <div className="chat-body">
                  <h6>Alexander Herthic</h6>
                  <AnswerChat
                    username="Alexander Herthic"
                    isMyAnswer={false}
                    message=" Lorem ipsum dolor amet, consectetur adipisicing elit Lorem
                      ipsum dolor amet, consectetur adipisicing elit Lorem ipsum
                      dolor amet, consectetur adiping elit"
                    time="5 min ago"
                  />
                  <AnswerChat
                    username="Me"
                    isMyAnswer={true}
                    message=" Lorem ipsum dolor amet, consectetur adipisicing elit Lorem
                      ipsum dolor amet, consectetur adipisicing elit Lorem ipsum
                      dolor amet, consectetur adiping elit"
                    time="1 min ago"
                  />
                  <AnswerChat
                    username="Alexander Herthic"
                    isMyAnswer={false}
                    message=" Lorem ipsum dolor amet, consectetur adipisicing elit Lorem
                      ipsum dolor amet, consectetur adipisicing elit Lorem ipsum
                      dolor amet, consectetur adiping elit"
                    time="5 min ago"
                  />
                  <AnswerChat
                    username="Me"
                    isMyAnswer={true}
                    message=" Lorem ipsum dolor amet, consectetur adipisicing elit Lorem
                      ipsum dolor amet, consectetur adipisicing elit Lorem ipsum
                      dolor amet, consectetur adiping elit"
                    time="1 min ago"
                  />
                </div>
              </div>
            </SimpleBar>
            <div className="answer-add clearfix">
              <div className="input-group">
                <CFormInput
                  className="input-message"
                  placeholder="Type your message"
                />
                <CButton className="btn btn-primary">Send</CButton>
              </div>
            </div>
          </CCol>
        </CRow>
      </CForm>
    </CContainer>
  );
}

export default Chat;
