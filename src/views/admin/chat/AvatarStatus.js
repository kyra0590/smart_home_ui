import React from "react";
import { CImage } from "@coreui/react";
import { statusChat } from "../../../constants/AppConstants";
import avatar_demo from "../../../assets/images/avatars/no-avatar.png";

function AvatarStatus(props) {
  const status =
    props.status !== null ? "status " + statusChat[props.status] : "status";
  const avatar = props?.avatar ?? avatar_demo;
  return (
    <>
      <div className="avatar">
        <CImage src={avatar} />
        <div className={status}></div>
      </div>
      <div className="username">{props.username}</div>
      <div className="mood">{props.mood}</div>
      <div className="text">{props.message}</div>
    </>
  );
}

export default AvatarStatus;
