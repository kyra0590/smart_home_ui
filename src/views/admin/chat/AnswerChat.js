import React from "react";
import AvatarStatus from "./AvatarStatus";

function AnswerChat(props) {
  return (
    <div className={props.isMyAnswer === true ? "answer right" : "answer left"}>
      <AvatarStatus
        username={props.username}
        isMyanswer={props.status}
        message={props.message}
      />
      {props.time && <div className="time">{props.time}</div>}
    </div>
  );
}

export default AnswerChat;
