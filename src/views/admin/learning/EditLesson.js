import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PropTypes from "prop-types";
//FETCH
import LessonFetch from "src/fetch/LessonFetch";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CForm,
  CFormInput,
  CCard,
  CFormLabel,
  CImage,
  CFormTextarea,
} from "@coreui/react";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
import GroupQuestion from "./child_widget/GroupQuestion";
// CSS
import "react-toastify/dist/ReactToastify.css";
import QuestionFetch from "src/fetch/QuestionFetch";
import LessonDetailsFetch from "src/fetch/LessonDetailsFetch";
import AppConfig from "src/AppConfig";
import { MediaDialog } from "src/views/widgets/media/MediaDialog";

function EditLesson() {
  // get query params
  let { id } = useParams();

  let navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [orderNumber, setOrderNumber] = useState(0);
  const [height, setHeight] = useState(500);
  const [listQuestions, setListquestions] = useState([]);
  const [listQuestionsChosen, setListquestionschosen] = useState([]);
  const [questionName, setQuestionName] = useState("");
  const [isVisible, setIsVisible] = useState(false);

  const imagePreview = (image) => {
    if (!image) return <></>;
    return image.substring(0, 6) === "images" ? (
      <CImage fluid thumbnail src={AppConfig.storageUrl + image} />
    ) : (
      <CImage fluid thumbnail src={image} />
    );
  };

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    loadQuestions();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  useEffect(() => {
    if (id) loadLesson(id);
  }, [id]);

  async function loadQuestions(name) {
    await QuestionFetch.getAll({ name: name }).then((json) => {
      setListquestions(json?.data);
    });
  }

  const searchQuestionName = async (name) => {
    setQuestionName(name);
    await loadQuestions(name);
  };

  const openMedia = () => {
    setIsVisible(true);
  };

  const closeMedia = () => {
    setIsVisible(false);
  };

  const onFileSelected = (item) => {
    setImage(item);
  };

  const loadLesson = async (id) => {
    await LessonFetch.getLesson(id)
      .then(async (json) => {
        const lesson = json?.data;
        if (lesson) {
          setName(lesson?.name);
          setDescription(lesson?.description ?? "");
          setImage(lesson?.image ?? "");
          setOrderNumber(lesson?.order_number ?? 0);
          await LessonDetailsFetch.getAll({ lession_id: lesson?.id })
            .then((jsonD) => {
              const questionDetails = jsonD?.data;
              let details = [];
              if (questionDetails) {
                details = questionDetails.map((item) => item?.question);
              }

              setListquestionschosen(details);
            })
            .catch((exD) => console.log(exD));
        }
      })
      .catch((ex) => console.log(ex));
  };

  const onSaveLesson = async () => {
    setIsLoading(true);
    if (!id) {
      await LessonFetch.createLesson({
        name: name,
        questions: listQuestionsChosen,
        description: description,
        image: image,
        order_number: orderNumber,
      })
        .then((json) => {
          showToast("Create Successfully", false);
          setTimeout(() => navigate("/admin/lessons"), 1000);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
        });
    } else {
      await LessonFetch.editLesson(id, {
        name: name,
        questions: listQuestionsChosen,
        description: description,
        image: image,
        order_number: orderNumber,
      })
        .then((json) => {
          showToast("Edit Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Edit Failed", true);
        });
    }
    setIsLoading(false);
  };

  const addQuestion = (question) => {
    setListquestionschosen([...listQuestionsChosen, question]);
  };

  const removeQuestion = (index) => {
    let questions = listQuestionsChosen;
    if (index !== -1) {
      questions.splice(index, 1);
      setListquestionschosen([...questions]);
    }
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />
      <CRow className="mb-4">
        <CCol xs="12">
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => navigate("/admin/lessons")}
          >
            Back
          </CButton>
          <CLoadingButton
            isLoading={isLoading}
            color="success"
            style={{ color: "#fff" }}
            onClick={onSaveLesson}
            labelLoading="Saving..."
            labelButton="Save"
          />
        </CCol>
      </CRow>

      <CForm className="row">
        <CCard className="p-3">
          <CRow className="mb-3">
            <CCol lg={4} xs={12}>
              <CRow className="mb-3">
                <CCol xs={12} lg={12}>
                  <CFormLabel>Name</CFormLabel>
                  <CFormInput
                    placeholder="Lesson"
                    value={name}
                    onChange={(event) => {
                      setName(event?.target?.value);
                    }}
                  />
                </CCol>
              </CRow>
              <CRow>
                <CCol xs={12} lg={12}>
                  <CFormLabel>Url</CFormLabel>
                </CCol>
                <CCol xs={12} lg={9} className="mb-3">
                  <CFormInput
                    placeholder="Url"
                    value={image ?? ""}
                    onChange={(event) => {
                      setImage(event?.target?.value);
                    }}
                  />
                </CCol>
                <CCol xs={12} lg={3} className="mb-3">
                  <CButton
                    onClick={() => {
                      openMedia();
                    }}
                  >
                    Browse
                  </CButton>
                </CCol>
              </CRow>
            </CCol>
            <CCol lg={4} xs={12}>
              <CRow>
                <CCol
                  lg={6}
                  xs={12}
                  className="mb-3"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    paddingTop: 0,
                  }}
                >
                  <CFormLabel style={{ width: "100%" }}>Image</CFormLabel>
                  {imagePreview(image)}
                </CCol>
                <CCol
                  lg={6}
                  xs={12}
                  className="mb-3"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <CFormLabel>Order Number</CFormLabel>
                  <CFormInput
                    type="number"
                    value={orderNumber}
                    onChange={(event) => {
                      setOrderNumber(event?.target?.value);
                    }}
                  />
                </CCol>
              </CRow>
            </CCol>
            <CCol lg={4} xs={12}>
              <CFormLabel>Description</CFormLabel>
              <CFormTextarea
                rows="4"
                value={description}
                onChange={(event) => {
                  setDescription(event.target.value);
                }}
                style={{ resize: "none" }}
              />
            </CCol>
          </CRow>

          <CRow>
            <CCol lg={12} xs={12}>
              <GroupQuestion
                maxHeight={height}
                listQuestions={listQuestions}
                listQuestionsChosen={listQuestionsChosen}
                addQuestion={addQuestion}
                removeQuestion={removeQuestion}
                questionName={questionName}
                setQuestionName={searchQuestionName}
              />
            </CCol>
          </CRow>
        </CCard>
      </CForm>
      <MediaDialog
        title="Media"
        visible={isVisible}
        onClose={closeMedia}
        fileSelected={image ?? ""}
        onFileSelected={onFileSelected}
      />
    </CContainer>
  );
}

EditLesson.propTypes = {
  id: PropTypes.number,
};

export default EditLesson;
