import React from "react";
import PropTypes from "prop-types";
// COMPONENTS
import CIcon from "@coreui/icons-react";
import { cilPencil, cilTrash } from "@coreui/icons";
import {
  CButton,
  CTableDataCell,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";

const ListQuestionTypes = (props) => {
  const { isLoading, items, editQuestionType, delQuestionType } = props;
  return (
    <>
      {!isLoading &&
        items &&
        items.map((item, index) => (
          <CTableRow key={index}>
            <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
            <CTableDataCell>{item.name}</CTableDataCell>
            <CTableDataCell>{item.description}</CTableDataCell>
            <CTableDataCell>
              <CButton
                color="info"
                className="btn-color-white"
                onClick={() => {
                  if (editQuestionType) editQuestionType(item.id);
                }}
              >
                <CIcon icon={cilPencil} title="Edit"></CIcon>
              </CButton>
              <CButton
                color="danger"
                className="m-3 btn-color-white"
                onClick={() => {
                  if (delQuestionType) delQuestionType(item);
                }}
              >
                <CIcon icon={cilTrash} title="Del"></CIcon>
              </CButton>
            </CTableDataCell>
          </CTableRow>
        ))}
    </>
  );
};

ListQuestionTypes.propTypes = {
  isLoading: PropTypes.bool,
  items: PropTypes.array,
  editQuestionType: PropTypes.func,
  delQuestionType: PropTypes.func,
};

export default ListQuestionTypes;
