import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
// COMPONENTS
import {
  CButton,
  CCol,
  CContainer,
  CPagination,
  CPaginationItem,
  CRow,
  CSpinner,
  CTable,
  CTableBody,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import { ToastContainer, toast } from "react-toastify";
import SimpleBar from "simplebar-react";
// FETCH
import QuestionTypeFetch from "src/fetch/QuestionTypeFetch";
import ListQuestionTypes from "./ListQuestionTypes";
import { Modal } from "src/components/AdminComponents/commons/Modal";

function QuestionType(props) {
  let navigate = useNavigate();
  const [questionTypes, setQuestionTypes] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [titleModal, setTitleModal] = useState();
  const [bodyModal, setBodyModal] = useState();
  const [isVisible, setIsVisible] = useState(false);
  const [totalPage, setTotalPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [questionType, setQuestionType] = useState(null);
  const [height, setHeight] = useState(500);

  const loadQuestionTypes = async (page = 1, limit = 20) => {
    setIsLoading(true);
    await QuestionTypeFetch.getAll({ limit: limit, page: page })
      .then((json) => {
        setQuestionTypes(json?.data);
        setTotalPage(json?.last_page);
        setCurrentPage(json?.current_page);
      })
      .catch((ex) => {
        console.log(ex);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    loadQuestionTypes();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  const nextPage = (nextPage) => {
    loadQuestionTypes(nextPage);
  };

  const editQuestionType = (id) => {
    navigate("/admin/edit-question-type/" + id);
  };

  const delQuestionType = (item) => {
    setTitleModal("Delete " + item.name);
    setBodyModal("Are you sure ?");
    setIsVisible(true);
    setQuestionType(item);
  };

  const onConfirmDelAction = async () => {
    if (questionType) {
      await QuestionTypeFetch.delQuestionType(questionType.id)
        .then((json) => {
          showToast("Delete Successfully", false);
        })
        .catch((ex) => {
          showToast("Delete Failed", false);
        });
      loadQuestionTypes();
    }
    setIsVisible(false);
    setQuestionType(null);
  };

  const showPages = () => {
    let listPage = [];
    const end = currentPage + 5 > totalPage ? totalPage : currentPage + 5;
    const first = end - 5 > 0 ? end - 5 : 1;
    for (let index = first; index <= end; index++) {
      let active = index === currentPage;
      listPage.push(
        <CPaginationItem
          key={index}
          active={active}
          onClick={() => {
            console.log(index);
            nextPage(index);
          }}
        >
          {index}
        </CPaginationItem>
      );
    }
    return listPage;
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />
      <CRow className="mb-4 mr-4">
        <CCol xs="2">
          <CButton
            color="info"
            style={{ color: "#fff" }}
            onClick={() => navigate("/admin/new-question-type")}
          >
            New Question Type
          </CButton>
        </CCol>
        {isLoading && <CSpinner component="span" color="info" />}
      </CRow>
      <CRow>
        <CCol xs="12" mb="12" className="table-list  p-3">
          <SimpleBar style={{ padding: 20, maxHeight: height - 300 }}>
            <CTable align="middle" responsive="xxl" bordered>
              <CTableHead color="light">
                <CTableRow>
                  <CTableHeaderCell className="col-id">#</CTableHeaderCell>
                  <CTableHeaderCell>Name</CTableHeaderCell>
                  <CTableHeaderCell>Description</CTableHeaderCell>
                  <CTableHeaderCell className="col-action">
                    Action
                  </CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                <ListQuestionTypes
                  items={questionTypes}
                  isLoading={isLoading}
                  editQuestionType={editQuestionType}
                  delQuestionType={delQuestionType}
                />
              </CTableBody>
            </CTable>
          </SimpleBar>
          {!isLoading && questionTypes?.length > 0 && (
            <CPagination align="end" aria-label="Page navigation example">
              <CPaginationItem
                disabled={currentPage === 1}
                onClick={() => {
                  nextPage(currentPage === 1 ? 1 : currentPage - 1);
                }}
              >
                <span>&laquo;</span>
              </CPaginationItem>
              {showPages()}
              <CPaginationItem
                disabled={currentPage === totalPage}
                onClick={() => {
                  nextPage(currentPage + 1);
                }}
              >
                <span>&raquo;</span>
              </CPaginationItem>
            </CPagination>
          )}
        </CCol>
      </CRow>
      <Modal
        visible={isVisible}
        onClose={setIsVisible}
        title={titleModal}
        body={bodyModal}
        onSave={onConfirmDelAction}
      />
    </CContainer>
  );
}

export default QuestionType;
