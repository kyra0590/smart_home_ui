import React from "react";
import CIcon from "@coreui/icons-react";
import PropTypes from "prop-types";
// COMPONENTS
import { cilPencil, cilTrash } from "@coreui/icons";
import {
  CButton,
  CTableDataCell,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";

const ListQuestions = (props) => {
  const { isLoading, items, editQuestion, delQuestion } = props;
  return (
    <>
      {!isLoading &&
        items &&
        items.map((item, index) => (
          <CTableRow key={index}>
            <CTableHeaderCell scope="row">{item.id}</CTableHeaderCell>
            <CTableDataCell>{item.name}</CTableDataCell>
            <CTableDataCell>{item?.question_type?.name}</CTableDataCell>
            <CTableDataCell>{item?.order_number}</CTableDataCell>
            <CTableDataCell>{item.description}</CTableDataCell>
            <CTableDataCell>
              <CButton
                color="info"
                className="btn-color-white"
                onClick={() => {
                  if (editQuestion) editQuestion(item.id);
                }}
              >
                <CIcon icon={cilPencil} title="Edit"></CIcon>
              </CButton>
              <CButton
                color="danger"
                className="m-3 btn-color-white"
                onClick={() => {
                  if (delQuestion) delQuestion(item);
                }}
              >
                <CIcon icon={cilTrash} title="Del"></CIcon>
              </CButton>
            </CTableDataCell>
          </CTableRow>
        ))}
    </>
  );
};

ListQuestions.propTypes = {
  isLoading: PropTypes.bool,
  items: PropTypes.array,
  editQuestion: PropTypes.func,
  delQuestion: PropTypes.func,
};

export default ListQuestions;
