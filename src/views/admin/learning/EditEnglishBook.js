import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PropTypes from "prop-types";
//FETCH
import EnglishBookFetch from "src/fetch/EnglishBookFetch";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CForm,
  CFormInput,
  CCard,
  CFormLabel,
  CFormTextarea,
  CImage,
} from "@coreui/react";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
// CSS
import "react-toastify/dist/ReactToastify.css";
import GroupLesson from "./child_widget/GroupLesson";
import LessonFetch from "src/fetch/LessonFetch";
import EnglishBookDetailsFetch from "src/fetch/EnglishBookDetailsFetch";
import { MediaDialog } from "src/views/widgets/media/MediaDialog";
import AppConfig from "src/AppConfig";

function EditEnglishBook(props) {
  // get query params
  let { id } = useParams();

  let navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [orderNumber, setOrderNumber] = useState(0);
  const [height, setHeight] = useState(500);
  const [listLessons, setListLessons] = useState([]);
  const [listLessonsChosen, setListLessonsChosen] = useState([]);
  const [lessonName, setLessonName] = useState("");
  const [isVisible, setIsVisible] = useState(false);

  const imagePreview = (image) => {
    if (!image) return <></>;
    return image.substring(0, 6) === "images" ? (
      <CImage fluid thumbnail src={AppConfig.storageUrl + image} />
    ) : (
      <CImage fluid thumbnail src={image} />
    );
  };

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    loadLessons();
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  async function loadLessons(name = "") {
    await LessonFetch.getAll({ name: name }).then((json) => {
      setListLessons(json?.data);
    });
  }

  const handleChangeLessonName = async (name) => {
    setLessonName(name);
    await loadLessons(name);
  };

  const addLesson = (lesson) => {
    setListLessonsChosen([...listLessonsChosen, lesson]);
  };

  const removeLesson = (index) => {
    let lessons = listLessonsChosen;
    if (index !== -1) {
      lessons.splice(index, 1);
      setListLessonsChosen([...lessons]);
    }
  };

  const openMedia = () => {
    setIsVisible(true);
  };

  const closeMedia = () => {
    setIsVisible(false);
  };

  const onFileSelected = (item) => {
    setImage(item);
  };

  const loadEnglishBook = async (id) => {
    await EnglishBookFetch.getEnglishBook(id)
      .then(async (json) => {
        const book = json?.data;
        if (book) {
          setName(book?.name);
          setDescription(book?.description ?? "");
          setImage(book?.image ?? "");
          setOrderNumber(book?.order_number ?? 0);

          await EnglishBookDetailsFetch.getAll({ english_book_id: book?.id })
            .then((jsonD) => {
              const englishBookDetails = jsonD?.data;
              let details = [];
              if (englishBookDetails) {
                details = englishBookDetails.map((item) => {
                  return item?.lesson;
                });
              }
              setListLessonsChosen(details);
            })
            .catch((exD) => console.log(exD));
        }
      })
      .catch((ex) => console.log(ex));
  };

  useEffect(() => {
    if (id) loadEnglishBook(id);
  }, [id]);

  const onSaveEnglishBook = async () => {
    setIsLoading(true);
    if (!id) {
      await EnglishBookFetch.createEnglishBook({
        name: name,
        description: description,
        lessons: listLessonsChosen,
        image: image,
        order_number: orderNumber,
      })
        .then((json) => {
          showToast("Create Successfully", false);
          setTimeout(() => navigate("/admin/english-books"), 1000);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
        });
    } else {
      await EnglishBookFetch.editEnglishBook(id, {
        name: name ?? "",
        description: description ?? "",
        lessons: listLessonsChosen,
        image: image,
        order_number: orderNumber,
      })
        .then((json) => {
          showToast("Edit Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Edit Failed", true);
        });
    }
    setIsLoading(false);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />

      <CRow className="mb-4">
        <CCol xs="12">
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => navigate("/admin/english-books")}
          >
            Back
          </CButton>
          <CLoadingButton
            isLoading={isLoading}
            color="success"
            style={{ color: "#fff" }}
            onClick={onSaveEnglishBook}
            labelLoading="Saving..."
            labelButton="Save"
          />
        </CCol>
      </CRow>

      <CForm className="row">
        <CCard className="p-3">
          <CRow className="mb-3">
            <CCol lg={4} xs={12}>
              <CRow className="mb-3">
                <CCol xs={12} lg={12}>
                  <CFormLabel>Name</CFormLabel>
                  <CFormInput
                    placeholder="Book"
                    value={name}
                    onChange={(event) => {
                      setName(event?.target?.value);
                    }}
                  />
                </CCol>
              </CRow>
              <CRow>
                <CCol xs={12} lg={12}>
                  <CFormLabel>Url</CFormLabel>
                </CCol>
                <CCol xs={12} lg={9} className="mb-3">
                  <CFormInput
                    placeholder="Url"
                    value={image ?? ""}
                    onChange={(event) => {
                      setImage(event?.target?.value);
                    }}
                  />
                </CCol>
                <CCol xs={12} lg={3} className="mb-3">
                  <CButton
                    onClick={() => {
                      openMedia();
                    }}
                  >
                    Browse
                  </CButton>
                </CCol>
              </CRow>
            </CCol>
            <CCol lg={4} xs={12}>
              <CRow>
                <CCol
                  lg={6}
                  xs={12}
                  className="mb-3"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    paddingTop: 0,
                  }}
                >
                  <CFormLabel style={{ width: "100%" }}>Image</CFormLabel>
                  {imagePreview(image)}
                </CCol>
                <CCol
                  lg={6}
                  xs={12}
                  className="mb-3"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <CFormLabel>Order Number</CFormLabel>
                  <CFormInput
                    type="number"
                    value={orderNumber}
                    onChange={(event) => {
                      setOrderNumber(event?.target?.value);
                    }}
                  />
                </CCol>
              </CRow>
            </CCol>
            <CCol lg={4} xs={12}>
              <CRow className="p-2">
                <CFormLabel>Description</CFormLabel>
                <CFormTextarea
                  rows="4"
                  value={description}
                  onChange={(event) => {
                    setDescription(event.target.value);
                  }}
                  style={{ resize: "none" }}
                />
              </CRow>
            </CCol>
          </CRow>

          <CRow>
            <CCol lg={12} xs={12}>
              <GroupLesson
                maxHeight={height}
                listLessons={listLessons}
                listLessonsChosen={listLessonsChosen}
                addLesson={addLesson}
                removeLesson={removeLesson}
                lessonName={lessonName}
                setLessonName={handleChangeLessonName}
              />
            </CCol>
          </CRow>
        </CCard>
      </CForm>
      <MediaDialog
        title="Media"
        visible={isVisible}
        onClose={closeMedia}
        fileSelected={image ?? ""}
        onFileSelected={onFileSelected}
      />
    </CContainer>
  );
}

EditEnglishBook.propTypes = {
  id: PropTypes.number,
};

export default EditEnglishBook;
