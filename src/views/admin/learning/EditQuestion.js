import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PropTypes from "prop-types";
//FETCH
import QuestionFetch from "src/fetch/QuestionFetch";
import QuestionTypeFetch from "src/fetch/QuestionTypeFetch";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CForm,
  CFormInput,
  CCard,
  CFormLabel,
  CFormTextarea,
  CFormSelect,
} from "@coreui/react";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
// CSS
import "react-toastify/dist/ReactToastify.css";
import QuestionWidget from "./child_widget/QuestionWidget";
import AnswerDetailsFetch from "src/fetch/AnswerDetailsFetch";

function EditQuestion() {
  // get query params
  let { id } = useParams();

  let navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [orderNumber, setOrderNumber] = useState(0);
  const [, setQuestion] = useState(null);
  const [questionType, setQuestionType] = useState(1);
  const [types, setTypes] = useState(null);
  const [answers, setAnswers] = useState([]);
  const [height, setHeight] = useState(500);

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    const loadQuestionTypes = async () => {
      await QuestionTypeFetch.getAll()
        .then((json) => {
          const types = json?.data;
          setTypes(types);
        })
        .catch((ex) => console.log("error load question type : ", ex));
    };

    loadQuestionTypes();

    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  useEffect(() => {
    if (id) loadQuestion(id);
  }, [id]);

  useEffect(() => {
    if (questionType) {
      setAnswers([]);
    }
  }, [questionType, setAnswers]);

  const addMoreAnswers = () => {
    let listAnswers = answers;
    listAnswers.push({
      name: "",
      image: "",
      questions: "",
      answers: "",
      is_external_image: false,
      description: "",
    });
    setAnswers([...listAnswers]);
  };

  const loadQuestion = async (id) => {
    await QuestionFetch.getQuestion(id)
      .then(async (json) => {
        const question = json?.data;
        if (question) {
          setName(question?.name);
          setDescription(question?.description ?? "");
          setQuestion(question);
          setQuestionType(question?.question_type_id ?? 0);
          setOrderNumber(question?.order_number ?? 0);
          // load list answers
          await AnswerDetailsFetch.getAll({
            question_id: id,
            order_field: "id",
            order_direction: "asc",
          })
            .then((json) => {
              const answersList = json?.data.map((item) => {
                return {
                  image: item?.image ?? "",
                  is_external_image: item?.is_external_image ?? false,
                  name: item?.name ?? "",
                  questions: item?.questions ?? "",
                  answers: item?.answers ?? "",
                  description: item?.description ?? "",
                };
              });
              setAnswers(answersList);
            })
            .catch((ex) => {
              console.log(ex);
            });
        }
      })
      .catch((ex) => console.log(ex));
  };

  const onSaveQuestion = async () => {
    setIsLoading(true);
    if (!id) {
      await QuestionFetch.createQuestion({
        name: name,
        description: description,
        question_type_id: questionType,
        order_number: orderNumber,
      })
        .then(async (json) => {
          if (json.question) {
            await AnswerDetailsFetch.createAnswers(
              { answers: answers },
              json.question?.id
            )
              .then((jsonA) => {
                console.log(jsonA);
              })
              .catch((exA) => {
                console.log(exA);
              });
          }

          showToast("Create Successfully", false);
          setTimeout(() => navigate("/admin/questions"), 1000);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
        });
    } else {
      await QuestionFetch.editQuestion(id, {
        name: name ?? "",
        description: description ?? "",
        question_type_id: questionType ?? "",
        order_number: orderNumber ?? "",
      })
        .then(async (json) => {
          showToast("Edit Successfully", false);
          await AnswerDetailsFetch.createAnswers({ answers: answers }, id)
            .then((jsonA) => {
              console.log(jsonA);
            })
            .catch((exA) => {
              console.log(exA);
            });
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Edit Failed", true);
        });
    }
    setIsLoading(false);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />

      <CRow className="mb-4">
        <CCol xs={12} lg={12}>
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => navigate("/admin/questions")}
          >
            Back
          </CButton>
          <CLoadingButton
            isLoading={isLoading}
            color="success"
            style={{ color: "#fff" }}
            onClick={onSaveQuestion}
            labelLoading="Saving..."
            labelButton="Save"
          />
        </CCol>
      </CRow>
      <CForm className="row">
        <CCard className="p-3">
          <CRow className="mb-3">
            <CCol xs={12} lg={4} className="mb-3">
              <CFormLabel>Name</CFormLabel>
              <CFormInput
                placeholder="Question"
                value={name}
                onChange={(event) => {
                  setName(event?.target?.value);
                }}
              />
            </CCol>
            <CCol xs={12} lg={5}>
              <CRow>
                <CCol xs={12} lg={6} className="mb-3">
                  <CFormLabel>Type</CFormLabel>
                  <CFormSelect
                    value={questionType}
                    onChange={(event) => {
                      setQuestionType(parseInt(event.target.value));
                    }}
                  >
                    <option value="0">Selection...</option>
                    {types &&
                      types.map((type) => (
                        <option key={type.id} value={type.id}>
                          {type.name}
                        </option>
                      ))}
                  </CFormSelect>
                </CCol>
                <CCol xs={12} lg={6} className="mb-3">
                  <CFormLabel>Order Number</CFormLabel>
                  <CFormInput
                    type="number"
                    placeholder="Number"
                    value={orderNumber}
                    onChange={(event) => {
                      setOrderNumber(event?.target?.value);
                    }}
                  />
                </CCol>
              </CRow>
            </CCol>
            <CCol xs={12} lg={3} className="mb-3">
              <CFormLabel>Description</CFormLabel>
              <CFormTextarea
                rows="2"
                value={description}
                onChange={(event) => {
                  setDescription(event.target.value);
                }}
                style={{ resize: "none" }}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xs={12}>
              <QuestionWidget
                questionType={questionType}
                answers={answers}
                addMoreAnswers={addMoreAnswers}
                setAnswers={setAnswers}
                maxHeight={height - 480}
              />
            </CCol>
          </CRow>
        </CCard>
      </CForm>
    </CContainer>
  );
}

EditQuestion.propTypes = {
  id: PropTypes.number,
};

export default EditQuestion;
