import React from "react";
import PropTypes from "prop-types";
// COMPONENTS
import CIcon from "@coreui/icons-react";
import { cilPencil, cilTrash } from "@coreui/icons";
import {
  CButton,
  CImage,
  CTableDataCell,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import AppConfig from "src/AppConfig";

const ListEnglishBooks = (props) => {
  const { isLoading, items, editEnglishBook, delEnglishBook } = props;

  const imagePreview = (image) => {
    if (!image) return <></>;

    return image.substring(0, 6) === "images" ? (
      <CImage width={100} fluid thumbnail src={AppConfig.storageUrl + image} />
    ) : (
      <CImage width={100} fluid thumbnail src={image} />
    );
  };

  return (
    <>
      {!isLoading &&
        items &&
        items.map((item, index) => (
          <CTableRow key={index}>
            <CTableHeaderCell scope="row">{item?.id}</CTableHeaderCell>
            <CTableDataCell>{item?.name}</CTableDataCell>
            <CTableDataCell style={{ textAlign: "center" }}>
              {imagePreview(item?.image)}
            </CTableDataCell>
            <CTableDataCell>{item?.order_number}</CTableDataCell>
            <CTableDataCell>{item?.description}</CTableDataCell>
            <CTableDataCell>
              <CButton
                color="info"
                className="btn-color-white"
                onClick={() => {
                  if (editEnglishBook) editEnglishBook(item?.id);
                }}
              >
                <CIcon icon={cilPencil} title="Edit"></CIcon>
              </CButton>
              <CButton
                color="danger"
                className="m-3 btn-color-white"
                onClick={() => {
                  if (delEnglishBook) delEnglishBook(item);
                }}
              >
                <CIcon icon={cilTrash} title="Del"></CIcon>
              </CButton>
            </CTableDataCell>
          </CTableRow>
        ))}
    </>
  );
};

ListEnglishBooks.propTypes = {
  isLoading: PropTypes.bool,
  items: PropTypes.array,
  editEnglishBook: PropTypes.func,
  delEnglishBook: PropTypes.func,
};

export default ListEnglishBooks;
