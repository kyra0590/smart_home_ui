import React from "react";
import PropTypes from "prop-types";
import { CCol, CRow, CButton } from "@coreui/react";
import AnswerImageTextItem from "./AnswerImageTextItem";
import SimpleBar from "simplebar-react";
function AnswerImageText(props) {
  const { answers, addMoreAnswers, setAnswers, maxHeight } = props;

  const editAnswer = (value, index, keyName) => {
    let answer = answers[index];
    if (answer) {
      switch (keyName) {
        case "name":
          answer.name = value;
          break;
        case "description":
          answer.description = value;
          break;
        case "questions":
          answer.questions = value;
          break;
        case "answers":
          answer.answers = value;
          break;
        case "image":
          answer.image = value;
          break;
        case "is_external_image":
          answer.is_external_image = value;
          break;
        default:
          break;
      }
      answers[index] = answer;
      setAnswers([...answers]);
    }
  };

  const removeAnswer = (index) => {
    let newAnswers = answers;
    if (index !== -1) {
      newAnswers.splice(index, 1);
    }
    setAnswers([...newAnswers]);
  };

  return (
    <>
      <CRow>
        <CCol xs={12} className="mb-3">
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => {
              addMoreAnswers();
            }}
          >
            Add Questions & Answers
          </CButton>
        </CCol>
        <SimpleBar style={{ maxHeight: maxHeight }}>
          {answers &&
            answers.map((answer, index) => (
              <AnswerImageTextItem
                key={index}
                answer={answer}
                index={index}
                editAnswer={editAnswer}
                removeAnswer={removeAnswer}
              />
            ))}
        </SimpleBar>
      </CRow>
    </>
  );
}

AnswerImageText.propTypes = {
  answers: PropTypes.array,
  addMoreAnswers: PropTypes.func,
  setAnswers: PropTypes.func,
  maxHeight: PropTypes.number,
};

export default AnswerImageText;
