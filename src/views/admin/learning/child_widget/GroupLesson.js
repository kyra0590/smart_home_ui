import CIcon from "@coreui/icons-react";
import PropTypes from "prop-types";
import {
  CCard,
  CCardBody,
  CCardSubtitle,
  CCardTitle,
  CCol,
  CFormInput,
  CListGroup,
  CListGroupItem,
  CRow,
} from "@coreui/react";
import { cilArrowRight, cilArrowThickRight, cilDelete } from "@coreui/icons";
import SimpleBar from "simplebar-react";

function GroupLesson(props) {
  const {
    maxHeight,
    listLessons,
    listLessonsChosen,
    addLesson,
    removeLesson,
    lessonName,
    setLessonName,
  } = props;

  return (
    <CRow>
      <CCol xs={12} lg={5}>
        <CCard style={{ height: maxHeight - 400 }}>
          <CCardBody>
            <CCardTitle className="mb-3">List Lessons</CCardTitle>
            <CCardSubtitle className="mb-2 text-medium-emphasis">
              <CFormInput
                placeholder="Search lessons"
                value={lessonName}
                onChange={(event) => setLessonName(event.target.value)}
              />
            </CCardSubtitle>
            <SimpleBar style={{ padding: 20, maxHeight: maxHeight - 530 }}>
              <CListGroup flush>
                {listLessons &&
                  listLessons.map((item, index) => (
                    <CListGroupItem
                      className="d-flex justify-content-between align-items-center"
                      key={index}
                    >
                      {item.name.length > 60 &&
                        item.name.substring(0, 45) + "..."}

                      {item.name.length <= 60 && item.name}

                      <CIcon
                        size="xxl"
                        icon={cilArrowThickRight}
                        onClick={() => {
                          if (addLesson) addLesson(item);
                        }}
                      ></CIcon>
                    </CListGroupItem>
                  ))}
              </CListGroup>
            </SimpleBar>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol
        xs={12}
        lg={2}
        className="p-3 d-flex justify-content-center align-items-center"
      >
        <CIcon icon={cilArrowRight} size="3xl"></CIcon>
      </CCol>
      <CCol xs={12} lg={5}>
        <CCard style={{ height: maxHeight - 400 }}>
          <CCardBody>
            <CCardTitle style={{ marginBottom: 58 }}>Chosen Lessons</CCardTitle>
            <SimpleBar style={{ padding: 20, maxHeight: maxHeight - 530 }}>
              <CListGroup flush>
                {listLessonsChosen &&
                  listLessonsChosen.map((item, index) => (
                    <CListGroupItem
                      className="d-flex justify-content-between align-items-center"
                      key={index}
                    >
                      {item?.name?.length > 60 &&
                        item?.name.substring(0, 60) + "..."}

                      {item?.name?.length <= 60 && item.name}
                      <CIcon
                        size="xxl"
                        icon={cilDelete}
                        onClick={() => {
                          if (removeLesson) removeLesson(index);
                        }}
                      ></CIcon>
                    </CListGroupItem>
                  ))}
              </CListGroup>
            </SimpleBar>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}

GroupLesson.propTypes = {
  maxHeight: PropTypes.number,
  listLessons: PropTypes.array,
  listLessonsChosen: PropTypes.array,
  addLesson: PropTypes.func,
  removeLesson: PropTypes.func,
  lessonName: PropTypes.string,
  setLessonName: PropTypes.func,
};

export default GroupLesson;
