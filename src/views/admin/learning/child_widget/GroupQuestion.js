import CIcon from "@coreui/icons-react";
import PropTypes from "prop-types";
import {
  CCard,
  CCardBody,
  CCardSubtitle,
  CCardTitle,
  CCol,
  CFormInput,
  CListGroup,
  CListGroupItem,
  CRow,
} from "@coreui/react";
import { cilArrowRight, cilArrowThickRight, cilDelete } from "@coreui/icons";
import SimpleBar from "simplebar-react";

function GroupQuestion(props) {
  const {
    maxHeight,
    listQuestions,
    listQuestionsChosen,
    addQuestion,
    removeQuestion,
    questionName,
    setQuestionName,
  } = props;

  return (
    <CRow>
      <CCol xs={12} lg={5}>
        <CCard style={{ height: maxHeight - 400 }}>
          <CCardBody>
            <CCardTitle className="mb-3">List Questions</CCardTitle>
            <CCardSubtitle className="mb-2 text-medium-emphasis">
              <CFormInput
                placeholder="Search questions"
                value={questionName}
                onChange={(event) => {
                  if (setQuestionName) setQuestionName(event?.target?.value);
                }}
              />
            </CCardSubtitle>
            <SimpleBar style={{ padding: 20, maxHeight: maxHeight - 530 }}>
              <CListGroup flush>
                {listQuestions &&
                  listQuestions.map((item, index) => (
                    <CListGroupItem
                      className="d-flex justify-content-between align-items-center"
                      key={index}
                    >
                      {item.name.length > 60 &&
                        item.name.substring(0, 45) + "..."}

                      {item.name.length <= 60 && item.name}

                      <CIcon
                        size="xxl"
                        icon={cilArrowThickRight}
                        onClick={() => {
                          if (addQuestion) addQuestion(item);
                        }}
                      ></CIcon>
                    </CListGroupItem>
                  ))}
              </CListGroup>
            </SimpleBar>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol
        xs={12}
        lg={2}
        className="p-3 d-flex justify-content-center align-items-center"
      >
        <CIcon icon={cilArrowRight} size="3xl"></CIcon>
      </CCol>
      <CCol xs={12} lg={5}>
        <CCard style={{ height: maxHeight - 400 }}>
          <CCardBody>
            <CCardTitle style={{ marginBottom: 58 }}>
              Chosen Questions
            </CCardTitle>
            <SimpleBar style={{ padding: 20, maxHeight: maxHeight - 530 }}>
              <CListGroup flush>
                {listQuestionsChosen &&
                  listQuestionsChosen.map((item, index) => (
                    <CListGroupItem
                      className="d-flex justify-content-between align-items-center"
                      key={index}
                    >
                      {item?.name?.length > 60 &&
                        item?.name.substring(0, 60) + "..."}

                      {item?.name?.length <= 60 && item.name}
                      <CIcon
                        size="xxl"
                        icon={cilDelete}
                        onClick={() => {
                          if (removeQuestion) removeQuestion(index);
                        }}
                      ></CIcon>
                    </CListGroupItem>
                  ))}
              </CListGroup>
            </SimpleBar>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}

GroupQuestion.propTypes = {
  maxHeight: PropTypes.number,
  listQuestions: PropTypes.array,
  listQuestionsChosen: PropTypes.array,
  addQuestion: PropTypes.func,
  removeQuestion: PropTypes.func,
  questionName: PropTypes.string,
  setQuestionName: PropTypes.func,
};

export default GroupQuestion;
