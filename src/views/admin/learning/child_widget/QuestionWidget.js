import React from "react";
import PropTypes from "prop-types";
// CONSTANTS
import { typeImageText } from "src/constants/AppConstants";
// COMPONENTS
import { CCol, CRow } from "@coreui/react";
import AnswerImageText from "./AnswerImageText";

function QuestionWidget(props) {
  const switchWidget = (questionType) => {
    const { answers, addMoreAnswers, setAnswers, maxHeight } = props;
    switch (questionType) {
      case typeImageText:
        return (
          <AnswerImageText
            answers={answers}
            addMoreAnswers={addMoreAnswers}
            setAnswers={setAnswers}
            maxHeight={maxHeight}
          />
        );
      default:
        return null;
    }
  };

  return (
    <>
      <CRow>
        <CCol>{switchWidget(props?.questionType)}</CCol>
      </CRow>
    </>
  );
}

QuestionWidget.propTypes = {
  questionType: PropTypes.number,
  answers: PropTypes.array,
  addMoreAnswers: PropTypes.func,
  setAnswers: PropTypes.func,
};

export default QuestionWidget;
