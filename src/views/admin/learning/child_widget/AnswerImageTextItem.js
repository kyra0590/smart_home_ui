import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
// COMPONENTS
import {
  CCol,
  CFormInput,
  CFormLabel,
  CImage,
  CRow,
  CFormTextarea,
  CButton,
} from "@coreui/react";
import { MediaDialog } from "src/views/widgets/media/MediaDialog";
import AppConfig from "src/AppConfig";
import CIcon from "@coreui/icons-react";
import { cilX } from "@coreui/icons";

function AnswerImageTextItem(props) {
  const { answer, index, editAnswer, removeAnswer } = props;
  const [isVisible, setIsVisible] = useState(false);
  const isExternalImge = answer?.is_external_image ? true : false;

  const imagePreview =
    isExternalImge === true
      ? answer?.image
      : AppConfig.storageUrl + answer?.image;

  const openMedia = () => {
    setIsVisible(true);
  };

  const closeMedia = () => {
    setIsVisible(false);
  };

  const onFileSelected = (item) => {
    if (editAnswer) {
      editAnswer(item, index, "image");
    }
  };

  useEffect(() => {
    if (editAnswer && answer?.image) {
      if (answer?.image.substring(0, 6) === "images") {
        editAnswer(false, index, "is_external_image");
      } else {
        editAnswer(true, index, "is_external_image");
      }
    }
  }, [editAnswer, answer, index]);

  return (
    <>
      <CRow>
        <CCol xs={12} lg={4}>
          <CRow>
            <CCol xs={12} lg={12} className="mb-3">
              <CFormLabel>Image name {index + 1}</CFormLabel>
              <CFormInput
                placeholder="Name"
                value={answer?.name ?? ""}
                onChange={(event) => {
                  editAnswer(event?.target?.value, index, "name");
                }}
              />
            </CCol>
          </CRow>
          <CRow>
            <CCol xs={12} lg={12}>
              <CFormLabel>Image</CFormLabel>
            </CCol>
            <CCol xs={12} lg={5} className="mb-3">
              <CFormInput
                placeholder="Url"
                value={answer?.image ?? ""}
                onChange={(event) => {
                  editAnswer(event?.target?.value, index, "image");
                }}
              />
            </CCol>
            <CCol xs={12} lg={3} className="mb-3">
              <CButton
                onClick={() => {
                  openMedia();
                }}
              >
                Browse
              </CButton>
            </CCol>
          </CRow>
        </CCol>
        <CCol xs={12} lg={2}>
          <CRow>
            <CFormLabel>Image</CFormLabel>
            <CCol xs={12} className="mb-3" style={{ margin: "auto" }}>
              {imagePreview && <CImage fluid thumbnail src={imagePreview} />}
            </CCol>
          </CRow>
        </CCol>
        <CCol xs={12} lg={3} className="mb-3">
          <CFormLabel>Questions</CFormLabel>
          <CFormTextarea
            rows="6"
            value={answer?.questions ?? ""}
            onChange={(event) => {
              editAnswer(event?.target?.value, index, "questions");
            }}
            style={{ resize: "none" }}
          />
        </CCol>
        <CCol xs={12} lg={3} className="mb-3">
          <CFormLabel>Answers</CFormLabel>
          <CFormTextarea
            rows="6"
            value={answer?.answers ?? ""}
            onChange={(event) => {
              editAnswer(event?.target?.value, index, "answers");
            }}
            style={{ resize: "none", width: "90%", display: "inline-block" }}
          />
          <CIcon
            icon={cilX}
            size="xl"
            style={{ float: "right", color: "red", cursor: "pointer" }}
            onClick={() => {
              removeAnswer(index);
            }}
          />
        </CCol>
        <CCol xs={12} lg={12} className="mb-3">
          <CFormLabel>Description</CFormLabel>
          <CFormTextarea
            rows="2"
            value={answer?.description ?? ""}
            onChange={(event) => {
              editAnswer(event?.target?.value, index, "description");
            }}
            style={{ resize: "none" }}
          />
        </CCol>
      </CRow>
      <MediaDialog
        title="Media"
        visible={isVisible}
        onClose={closeMedia}
        fileSelected={answer?.image ?? ""}
        onFileSelected={onFileSelected}
      />
    </>
  );
}

AnswerImageTextItem.propTypes = {
  answer: PropTypes.object,
  index: PropTypes.number,
  editAnswer: PropTypes.func,
  removeAnswer: PropTypes.func,
};

export default AnswerImageTextItem;
