import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
// COMPONENTS
import {
  CButton,
  CCol,
  CContainer,
  CPagination,
  CPaginationItem,
  CRow,
  CSpinner,
  CTable,
  CTableBody,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import { ToastContainer, toast } from "react-toastify";
import SimpleBar from "simplebar-react";
// FETCH
import LessonFetch from "src/fetch/LessonFetch";
import ListLesson from "./ListLesson";
import { Modal } from "src/components/AdminComponents/commons/Modal";
import {
  cilSortAscending,
  cilSortDescending,
  cilSwapVertical,
} from "@coreui/icons";
import CIcon from "@coreui/icons-react";

function Lesson() {
  let navigate = useNavigate();
  const [lessons, setLessons] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [titleModal, setTitleModal] = useState();
  const [bodyModal, setBodyModal] = useState();
  const [isVisible, setIsVisible] = useState(false);
  const [totalPage, setTotalPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [lesson, setLesson] = useState(null);
  const [height, setHeight] = useState(500);
  const [orderField, setOrderField] = useState("id");
  const [orderDirection, setOrderDirection] = useState("desc");

  const loadLesson = async ({
    page = 1,
    limit = 20,
    order_field = "id",
    order_direction = "desc",
  }) => {
    setIsLoading(true);
    await LessonFetch.getAll({
      limit: limit,
      page: page,
      order_field: order_field,
      order_direction: order_direction,
    })
      .then((json) => {
        setLessons(json?.data);
        setTotalPage(json?.last_page);
        setCurrentPage(json?.current_page);
      })
      .catch((ex) => {
        console.log(ex);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  useEffect(() => {
    loadLesson({
      order_field: orderField,
      order_direction: orderDirection,
      page: 1,
    });
    setCurrentPage(1);
  }, [orderField, orderDirection]);

  const nextPage = (nextPage) => {
    loadLesson({
      page: nextPage,
      order_field: orderField,
      order_direction: orderDirection,
    });
  };

  const editLesson = (id) => {
    navigate("/admin/edit-lesson/" + id);
  };

  const delLesson = (item) => {
    setTitleModal("Delete " + item.name);
    setBodyModal("Are you sure ?");
    setIsVisible(true);
    setLesson(item);
  };

  const onConfirmDelAction = async () => {
    if (lesson) {
      await LessonFetch.delLesson(lesson.id)
        .then((json) => {
          showToast("Delete Successfully", false);
        })
        .catch((ex) => {
          showToast("Delete Failed", false);
        });
      nextPage(1);
    }
    setIsVisible(false);
    setLesson(null);
  };

  const showPages = () => {
    let listPage = [];
    const end = currentPage + 5 > totalPage ? totalPage : currentPage + 5;
    const first = end - 5 > 0 ? end - 5 : 1;
    for (let index = first; index <= end; index++) {
      let active = index === currentPage;
      listPage.push(
        <CPaginationItem
          key={index}
          active={active}
          onClick={() => {
            console.log(index);
            nextPage(index);
          }}
        >
          {index}
        </CPaginationItem>
      );
    }
    return listPage;
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  const displayIconSort = (orderDirection) => {
    if (orderDirection === "asc") return cilSortAscending;
    if (orderDirection === "desc") return cilSortDescending;
    return cilSwapVertical;
  };

  const changeorderDirection = (direction) => {
    if (direction === "") return "desc";
    else if (direction === "asc") return "";
    return "asc";
  };

  const handleChangeSort = (fieldName) => {
    if (fieldName === orderField)
      setOrderDirection(changeorderDirection(orderDirection));
    else {
      setOrderDirection("desc");
    }
    setOrderField(fieldName);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />
      <CRow className="mb-4 mr-3">
        <CCol xs="2">
          <CButton
            color="info"
            style={{ color: "#fff" }}
            onClick={() => navigate("/admin/new-lesson")}
          >
            New Lesson
          </CButton>
        </CCol>
        {isLoading && <CSpinner component="span" color="info" />}
      </CRow>
      <CRow>
        <CCol xs="12" mb="12" className="table-list  p-3">
          <SimpleBar style={{ padding: 20, maxHeight: height - 300 }}>
            <CTable align="middle" responsive="xxl" bordered>
              <CTableHead color="light">
                <CTableRow>
                  <CTableHeaderCell className="col-id">
                    #
                    <CIcon
                      style={{ marginLeft: 10 }}
                      icon={
                        orderField === "id"
                          ? displayIconSort(orderDirection)
                          : cilSwapVertical
                      }
                      onClick={() => handleChangeSort("id")}
                    />
                  </CTableHeaderCell>
                  <CTableHeaderCell>
                    Name
                    <CIcon
                      style={{ marginLeft: 10 }}
                      icon={
                        orderField === "name"
                          ? displayIconSort(orderDirection)
                          : cilSwapVertical
                      }
                      onClick={() => handleChangeSort("name")}
                    />
                  </CTableHeaderCell>
                  <CTableHeaderCell style={{ width: 200 }}>
                    Image
                  </CTableHeaderCell>
                  <CTableHeaderCell style={{ width: 200 }}>
                    Order Number
                    <CIcon
                      style={{ marginLeft: 10 }}
                      icon={
                        orderField === "order_number"
                          ? displayIconSort(orderDirection)
                          : cilSwapVertical
                      }
                      onClick={() => handleChangeSort("order_number")}
                    />
                  </CTableHeaderCell>
                  <CTableHeaderCell>Description</CTableHeaderCell>
                  <CTableHeaderCell className="col-action">
                    Action
                  </CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                <ListLesson
                  items={lessons}
                  isLoading={isLoading}
                  editLesson={editLesson}
                  delLesson={delLesson}
                />
              </CTableBody>
            </CTable>
          </SimpleBar>
          {!isLoading && lessons?.length > 0 && (
            <CPagination align="end" aria-label="Page navigation example">
              <CPaginationItem
                disabled={currentPage === 1}
                onClick={() => {
                  nextPage(currentPage === 1 ? 1 : currentPage - 1);
                }}
              >
                <span>&laquo;</span>
              </CPaginationItem>
              {showPages()}
              <CPaginationItem
                disabled={currentPage === totalPage}
                onClick={() => {
                  nextPage(currentPage + 1);
                }}
              >
                <span>&raquo;</span>
              </CPaginationItem>
            </CPagination>
          )}
        </CCol>
      </CRow>
      <Modal
        visible={isVisible}
        onClose={setIsVisible}
        title={titleModal}
        body={bodyModal}
        onSave={onConfirmDelAction}
      />
    </CContainer>
  );
}

export default Lesson;
