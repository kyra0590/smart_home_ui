import React from "react";
import PropTypes from "prop-types";
// COMPONENTS
import CIcon from "@coreui/icons-react";
import { cilPencil, cilTrash } from "@coreui/icons";
import {
  CButton,
  CImage,
  CTableDataCell,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import AppConfig from "src/AppConfig";

const ListLesson = (props) => {
  const { isLoading, items, editLesson, delLesson } = props;

  const imagePreview = (image) => {
    if (!image) return <></>;

    return image.substring(0, 6) === "images" ? (
      <CImage width={100} fluid thumbnail src={AppConfig.storageUrl + image} />
    ) : (
      <CImage width={100} fluid thumbnail src={image} />
    );
  };

  return (
    <>
      {!isLoading &&
        items &&
        items.map((item, index) => (
          <CTableRow key={index}>
            <CTableHeaderCell scope="row">{item.id}</CTableHeaderCell>
            <CTableDataCell>{item.name}</CTableDataCell>
            <CTableDataCell style={{ textAlign: "center" }}>
              {imagePreview(item?.image)}
            </CTableDataCell>
            <CTableDataCell>{item.order_number}</CTableDataCell>
            <CTableDataCell>{item.description}</CTableDataCell>
            <CTableDataCell>
              <CButton
                color="info"
                className="btn-color-white"
                onClick={() => {
                  if (editLesson) editLesson(item.id);
                }}
              >
                <CIcon icon={cilPencil} title="Edit"></CIcon>
              </CButton>
              <CButton
                color="danger"
                className="m-3 btn-color-white"
                onClick={() => {
                  if (delLesson) delLesson(item);
                }}
              >
                <CIcon icon={cilTrash} title="Del"></CIcon>
              </CButton>
            </CTableDataCell>
          </CTableRow>
        ))}
    </>
  );
};

ListLesson.propTypes = {
  isLoading: PropTypes.bool,
  items: PropTypes.array,
  editLesson: PropTypes.func,
  delLesson: PropTypes.func,
};

export default ListLesson;
