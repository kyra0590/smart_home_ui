import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PropTypes from "prop-types";
//FETCH
import QuestionTypeFetch from "src/fetch/QuestionTypeFetch";
// COMPONENTS
import {
  CButton,
  CCol,
  CRow,
  CContainer,
  CForm,
  CFormInput,
  CCard,
  CFormLabel,
  CFormTextarea,
} from "@coreui/react";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
// CSS
import "react-toastify/dist/ReactToastify.css";

function EditQuestionType() {
  // get query params
  let { id } = useParams();

  let navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [, setQuestionType] = useState(null);

  useEffect(() => {
    if (id) loadQuestionType(id);
  }, [id]);

  const loadQuestionType = async (id) => {
    await QuestionTypeFetch.getQuestionType(id)
      .then(async (json) => {
        const questionType = json?.data;
        if (questionType) {
          setName(questionType?.name);
          setDescription(questionType?.description ?? "");
          setQuestionType(questionType);
        }
      })
      .catch((ex) => console.log(ex));
  };

  const onSaveQuestionType = async () => {
    setIsLoading(true);
    if (!id) {
      await QuestionTypeFetch.createQuestionType({
        name: name,
        description: description,
      })
        .then((json) => {
          showToast("Create Successfully", false);
          setTimeout(() => navigate("/admin/question-types"), 1000);
        })
        .catch((ex) => {
          showToast("Create Failed", true);
        });
    } else {
      await QuestionTypeFetch.editQuestionType(id, {
        name: name ?? "",
        description: description ?? "",
      })
        .then((json) => {
          showToast("Edit Successfully", false);
        })
        .catch((ex) => {
          console.log(ex);
          showToast("Edit Failed", true);
        });
    }
    setIsLoading(false);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <CContainer className="clearfix">
      <ToastContainer />

      <CRow className="mb-4">
        <CCol xs="12">
          <CButton
            color="dark"
            style={{ color: "#fff", marginRight: 10 }}
            onClick={() => navigate("/admin/question-types")}
          >
            Back
          </CButton>
          <CLoadingButton
            isLoading={isLoading}
            color="success"
            style={{ color: "#fff" }}
            onClick={onSaveQuestionType}
            labelLoading="Saving..."
            labelButton="Save"
          />
        </CCol>
      </CRow>

      <CForm className="row">
        <CCard className="p-3">
          <CRow className="mb-3">
            <CCol lg={4} xs={12}>
              <CFormLabel>Name</CFormLabel>
              <CFormInput
                placeholder="Question Type"
                value={name}
                onChange={(event) => {
                  setName(event?.target?.value);
                }}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol lg={4} xs={12}>
              <CFormLabel>Description</CFormLabel>
              <CFormTextarea
                rows="5"
                value={description}
                onChange={(event) => {
                  setDescription(event.target.value);
                }}
                style={{ resize: "none" }}
              />
            </CCol>
          </CRow>
        </CCard>
      </CForm>
    </CContainer>
  );
}

EditQuestionType.propTypes = {
  id: PropTypes.number,
};

export default EditQuestionType;
