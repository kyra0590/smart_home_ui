import React, { useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilLockLocked, cilUser } from "@coreui/icons";
import LoginFetch from "src/fetch/LoginFetch";
import { ToastContainer, toast } from "react-toastify";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { useNavigate } from "react-router-dom";

const Register = () => {
  const [name, setName] = useState("nguyen van a");
  const [password, setPassword] = useState("Abcd1234");
  const [passwordConfirmation, setPasswordConfirmation] = useState("Abcd1234");
  const [email, setEmail] = useState("test@yopmail.com");
  const [isLoading, setIsLoading] = useState(false);
  let navigate = useNavigate();

  const register = async () => {
    setIsLoading(true);
    await LoginFetch.register({
      name: name,
      password: password,
      password_confirmation: passwordConfirmation,
      email: email,
    })
      .then((json) => {
        showToast("Register Success", false);

        setTimeout(() => {
          window.location = "/admin";
        }, 1500);
      })
      .catch((ex) => {
        console.log(ex);
        showToast("Register Error", true);
      });
    setIsLoading(false);
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <ToastContainer />
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Register</h1>
                  <p className="text-medium-emphasis">Create your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      placeholder="Name"
                      autoComplete="name"
                      value={name}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>@</CInputGroupText>
                    <CFormInput
                      placeholder="Email"
                      autoComplete="email"
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Repeat password"
                      autoComplete="new-password"
                      onChange={(e) => {
                        setPasswordConfirmation(e.target.value);
                      }}
                    />
                  </CInputGroup>
                  <div className="d-grid mb-3">
                    <CLoadingButton
                      color="success"
                      isLoading={isLoading}
                      labelButton="Create Account"
                      labelLoading="Creating..."
                      onClick={() => register()}
                      style={{ color: "#fff" }}
                    />
                  </div>
                  <div className="d-grid">
                    <CButton
                      color="dark"
                      style={{ color: "#fff" }}
                      onClick={() => navigate("/admin/login")}
                    >
                      Cancel
                    </CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Register;
