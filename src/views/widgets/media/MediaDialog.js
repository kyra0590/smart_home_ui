import React, { useState } from "react";
import Media from "./Media";

const {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
} = require("@coreui/react");

export function MediaDialog(props) {
  const { visible, tilte, onFileSelected, onClose } = props;

  const [file, setFile] = useState("");

  return (
    <CModal
      size="lg"
      alignment="center"
      visible={visible}
      onClose={() => {
        if (onClose) {
          onClose();
        }
      }}
      style={{ maxHeight: "90%" }}
      scrollable={true}
    >
      <CModalHeader>
        <CModalTitle>{tilte}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <Media onFileSelected={setFile} fileSelected={file} />
      </CModalBody>
      <CModalFooter>
        <CButton
          color="success"
          style={{ color: "white" }}
          onClick={() => {
            if (onFileSelected) {
              onFileSelected(file);
            }

            if (onClose) {
              onClose();
            }
          }}
        >
          Choose
        </CButton>
        <CButton
          color="danger"
          style={{ color: "white" }}
          onClick={() => {
            setFile("");
            if (onFileSelected) {
              onFileSelected("");
            }
            if (onClose) {
              onClose();
            }
          }}
        >
          Cancel
        </CButton>
      </CModalFooter>
    </CModal>
  );
}
