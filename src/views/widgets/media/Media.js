import React, { useEffect, useState } from "react";
import {
  CButton,
  CCard,
  CCardImage,
  CCol,
  CFormCheck,
  CRow,
} from "@coreui/react";
// FETCH
import FilesFetch from "src/fetch/FilesFetch";
// CSS
import "react-tabs/style/react-tabs.css";
import { cilXCircle } from "@coreui/icons";
// CONFIG
import AppConfig from "src/AppConfig";

// COMPONENTS
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { useDropzone } from "react-dropzone";
import CLoadingButton from "src/components/AdminComponents/commons/CLoadingButton";
import { ToastContainer, toast } from "react-toastify";
import ReactPlayer from "react-player";
import SimpleBar from "simplebar-react";
import CIcon from "@coreui/icons-react";

function Media(props) {
  const [tabIndex, setTabIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [files, setFiles] = useState([]);
  const [images, setImages] = useState([]);
  const [videos, setVideos] = useState([]);
  const [imagesDelete, setImagesDelete] = useState([]);
  const [videosDelete, setVideosDelete] = useState([]);
  const [height, setHeight] = useState(300);
  const { fileSelected, onFileSelected } = props;

  const { getRootProps, getInputProps } = useDropzone({
    accept: {
      "image/*": ["png", "jpeg", "jpg", "gif"],
      "video/*": ["mp4"],
    },
    onDrop: (acceptedFiles) => {
      setFiles([...files, ...acceptedFiles]);
    },
  });

  const uploadFiles = async () => {
    setIsLoading(true);
    setTimeout(function () {}, 500);

    let formData = new FormData();
    for (var index = 0; index < files.length; index++) {
      formData.append("file" + index, files[index]);
    }

    await FilesFetch.upload(formData)
      .then((json) => {
        showToast(json?.message, false);
      })
      .catch((ex) => {
        showToast("Upload failed", true);
      });

    setIsLoading(false);
    setFiles([]);
    refetchMedia();
  };

  const loadListImages = async () => {
    await FilesFetch.listImages()
      .then((json) => {
        setImages(json?.images);
      })
      .catch((ex) => {
        console.log(ex);
      });
  };

  const loadListVideos = async () => {
    await FilesFetch.listVideos()
      .then((json) => {
        setVideos(json?.videos);
      })
      .catch((ex) => {
        console.log(ex);
      });
  };

  const removeItem = (index) => {
    var newFiles = [...files];
    if (index !== -1) {
      newFiles.splice(index, 1);
      setFiles(newFiles);
    }
  };

  const deleteItems = async (items) => {
    if (items.length > 0) {
      await FilesFetch.deleteItems({ files: items })
        .then((json) => {
          showToast(json?.message, false);
          refetchMedia();
          setImagesDelete([]);
          setVideosDelete([]);
        })
        .catch((ex) => {
          showToast("Delete files failed", true);
        });
    }
  };

  const thumbs = files.map((file, index) => (
    <CCol xs={12} lg={3} md={4} key={"thumb" + index} className="mb-3">
      <CCard className="p-2" color="">
        {file.type !== "video/mp4" && (
          <CCardImage
            orientation="top"
            src={URL.createObjectURL(file)}
            onLoad={() => {
              URL.revokeObjectURL(file.preview);
            }}
          />
        )}

        {file.type === "video/mp4" && (
          <ReactPlayer
            width="100%"
            height="100%"
            url={file ? URL.createObjectURL(file) : ""}
            controls={true}
          />
        )}
        <CIcon
          icon={cilXCircle}
          size="xxl"
          style={{
            position: "absolute",
            top: 10,
            right: 10,
            background: "white",
            borderRadius: "50%",
            cursor: "pointer",
          }}
          onClick={() => removeItem(index)}
        />
      </CCard>
    </CCol>
  ));

  const onImagesDelete = (status, item) => {
    let selectedImages = imagesDelete;
    console.log(selectedImages);
    if (status) {
      selectedImages.push(item);
    } else {
      const index = selectedImages.indexOf(item);
      if (index !== -1) {
        selectedImages.splice(index, 1);
      }
    }
    setImagesDelete([...selectedImages]);
  };

  const onVideosDelete = (status, item) => {
    let selectedVideos = videosDelete;
    if (status) {
      selectedVideos.push(item);
    } else {
      const index = selectedVideos.indexOf(item);
      if (index !== -1) {
        selectedVideos.splice(index, 1);
      }
    }
    setVideosDelete([...selectedVideos]);
  };

  const showImages = (images) => {
    return (
      images &&
      images.map((item, index) => (
        <CCol xs={12} lg={3} md={4} key={"image" + index} className="mb-3">
          <CCard
            className="p-2"
            onClick={(event) => {
              if (onFileSelected) {
                if (fileSelected !== "" && fileSelected === item) {
                  onFileSelected("");
                } else {
                  onFileSelected(item);
                }
              }
            }}
            color={fileSelected === item ? "danger" : ""}
          >
            <CFormCheck
              style={{
                position: "absolute",
                left: 15,
              }}
              onChange={(event) => {
                onImagesDelete(event?.target?.checked, item);
              }}
              checked={imagesDelete.includes(item) ? true : false}
            />
            <CIcon
              icon={cilXCircle}
              size="xxl"
              style={{
                position: "absolute",
                top: 10,
                right: 10,
                background: "white",
                borderRadius: "50%",
                cursor: "pointer",
              }}
              onClick={() => deleteItems([item])}
            />
            <CCardImage src={AppConfig.storageUrl + item} />
          </CCard>
        </CCol>
      ))
    );
  };

  const showVideos = (videos) => {
    return (
      videos &&
      videos.map((item, index) => (
        <CCol xs={12} lg={3} md={4} key={"video" + index} className="mb-3">
          <CCard className="p-2" color="">
            <CFormCheck
              style={{
                position: "absolute",
                left: 15,
                zIndex: 9999,
              }}
              onChange={(event) => {
                onVideosDelete(event?.target?.checked, item);
              }}
              checked={videosDelete.includes(item) ? true : false}
            />
            <CIcon
              icon={cilXCircle}
              size="xxl"
              style={{
                position: "absolute",
                top: 10,
                right: 10,
                background: "white",
                borderRadius: "50%",
                cursor: "pointer",
                zIndex: 9999,
              }}
              onClick={() => deleteItems([item])}
            />
            <ReactPlayer
              width="100%"
              height="100%"
              url={item ? AppConfig.storageUrl + item : ""}
              controls={true}
            />
          </CCard>
        </CCol>
      ))
    );
  };

  const showButton = (tabIndex) => {
    if (tabIndex === 0) {
      return (
        <>
          <CLoadingButton
            style={{ position: "absolute", right: 0, top: -5 }}
            isLoading={isLoading}
            onClick={() => {
              uploadFiles();
            }}
            labelButton="Upload files"
            labelLoading="Uploading..."
            disabled={files?.length > 0 ? false : true}
          />
          <CButton
            color="warning"
            style={{
              position: "absolute",
              right: 120,
              top: -5,
              color: "white",
            }}
            onClick={() => {
              setFiles([]);
            }}
            disabled={files?.length > 0 ? false : true}
          >
            Clear
          </CButton>
        </>
      );
    } else if (tabIndex === 1) {
      return (
        <CButton
          color="warning"
          style={{ position: "absolute", right: 0, top: -5, color: "white" }}
          onClick={() => {
            deleteItems(imagesDelete);
          }}
        >
          Delete Images
        </CButton>
      );
    } else {
      return (
        <CButton
          color="warning"
          style={{ position: "absolute", right: 0, top: -5, color: "white" }}
          onClick={() => {
            deleteItems(videosDelete);
          }}
        >
          Delete Images
        </CButton>
      );
    }
  };

  const showToast = (title, error) => {
    error ? toast.error(title) : toast.success(title);
  };

  function refetchMedia() {
    loadListImages();
    loadListVideos();
  }

  const updateWindowDimensions = () => {
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    loadListImages();
    loadListVideos();

    updateWindowDimensions();
    window.addEventListener("resize", updateWindowDimensions);
  }, []);

  return (
    <SimpleBar style={{ padding: 20, maxHeight: height - 200 }}>
      <Tabs defaultIndex={tabIndex} onSelect={(index) => setTabIndex(index)}>
        <ToastContainer />

        <TabList style={{ position: "relative" }}>
          <Tab>Add New</Tab>
          <Tab>Images</Tab>
          <Tab>Videos</Tab>
          {showButton(tabIndex)}
        </TabList>
        <TabPanel>
          <CCard
            style={{
              display: "flex",
              justifyContent: "center",
              alignContent: "center",
              textAlign: "center",
              padding: "40px 0",
            }}
            {...getRootProps({ className: "dropzone" })}
            className="mb-3"
          >
            <input {...getInputProps()} />
            Drag & drop some files here, or click to select files
          </CCard>
          <CRow>{thumbs}</CRow>
        </TabPanel>
        <TabPanel>
          <CRow>{showImages(images)}</CRow>
        </TabPanel>
        <TabPanel>
          <CRow>{showVideos(videos)}</CRow>
        </TabPanel>
      </Tabs>
    </SimpleBar>
  );
}

export default Media;
