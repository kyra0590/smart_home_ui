import React, { useState } from "react";
import PropTypes from "prop-types";
import AnswerDetailsFetch from "src/fetch/AnswerDetailsFetch";
import { useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardText,
  CCardTitle,
  CCol,
  CImage,
  CRow,
} from "@coreui/react";
import AppConfig from "src/AppConfig";
import Conversation from "./Conversation";
import IconQuestion from "src/assets/images/icon/icon_question.png";

const QuestionListItem = (props) => {
  const {
    question,
    speak,
    voices,
    voiceQuestion,
    voiceAnswer,
    timeDelay,
    speaking,
  } = props;
  const [answers, setAnswers] = useState();

  useEffect(() => {
    const loadAnswerDetail = async (question_id) => {
      await AnswerDetailsFetch.getAll({ question_id: question_id })
        .then((json) => {
          setAnswers(json?.data);
        })
        .catch((ex) => {
          console.log(ex);
        });
    };
    if (question) {
      loadAnswerDetail(question.id);
    }
  }, [question]);

  const previewImage = (url, name) => {
    if (!url) return <></>;

    return url.substring(0, 6) === "images" ? (
      <CImage
        fluid
        thumbnail
        src={AppConfig.storageUrl + url}
        className="mb-2"
        onClick={() => readText({ text: name, isQuestion: false })}
      ></CImage>
    ) : (
      <CImage
        fluid
        thumbnail
        src={url}
        className="mb-2"
        onClick={() => readText({ text: name, isQuestion: false })}
      ></CImage>
    );
  };

  const readText = ({ text, isQuestion = true }) => {
    let voice = isQuestion ? voices[voiceQuestion] : voices[voiceAnswer];
    speak({ text: text, voice: voice });
  };

  return (
    <div className="single_post">
      <h3 className="capitalize padding10 alert alert-primary">
        <CImage src={IconQuestion} width={30} className="me-2" />
        {question.name}
      </h3>
      <CRow>
        {answers && 
          answers.map((answer, index) => {
            return (
              <CCol key={index} xs={12} md={6} lg={4} className="mb-3">
                <CCard>
                  <CCardBody>
                    <CCardTitle>{answer.name}</CCardTitle>
                    {previewImage(answer.image, answer.name)}
                    {answer.description && (
                      <CCardText className="mb-2">
                        {answer.description}
                      </CCardText>
                    )}

                    <Conversation
                      questions={answer?.questions_arr}
                      answers={answer?.answers_arr}
                      readText={readText}
                      timeDelay={timeDelay}
                      speaking={speaking}
                    />
                  </CCardBody>
                </CCard>
              </CCol>
            );
          })}
      </CRow>
    </div>
  );
};

QuestionListItem.propTypes = {
  question: PropTypes.object,
};

export default QuestionListItem;
