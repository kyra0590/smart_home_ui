import React from "react";
import { Link } from "react-router-dom";
import AppConfig from "src/AppConfig";
import PropTypes from "prop-types";
import { CImage } from "@coreui/react";

const EnglishBooksListItem = (props) => {
  const { book } = props;

  const previewImage = (url) => {
    if (!url) return <></>;

    return url.substring(0, 6) === "images" ? (
      <CImage fluid thumbnail src={AppConfig.storageUrl + url}></CImage>
    ) : (
      <CImage fluid thumbnail src={url}></CImage>
    );
  };

  return (
    <div className="single_post post_type3 mb30">
      <div className="post_img">
        <div className="img_wrap">
          <Link to={`/english-book/${book?.id}`}>
            {previewImage(book?.image)}
          </Link>
        </div>
      </div>
      <div className="single_post_text">
        <div className="meta3">
          <Link to="/english-books">English Books</Link>
        </div>
        <h4>
          <Link to={`/english-book/${book?.id}`}>{book?.name}</Link>
        </h4>
        <div className="space-10" />
        <p className="post-p">{book?.description}</p>
      </div>
    </div>
  );
};

EnglishBooksListItem.propTypes = {
  book: PropTypes.object,
};

export default EnglishBooksListItem;
