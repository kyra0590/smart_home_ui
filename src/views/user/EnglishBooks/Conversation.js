import { useEffect, useState } from "react";
import { CListGroup, CListGroupItem, CButton } from "@coreui/react";

function Conversation(props) {
  const { questions, answers, readText, speaking } = props;
  const [conversation, setConversation] = useState();
  const [rowIndex, setRowIndex] = useState(-1);
  const [disable, setDisable] = useState(false);
  const [rowSelected, setRowSelected] = useState(-1);

  const readConversation = (index = 0) => {
    setRowSelected(-1);
    setTimeout(
      () => {
        setDisable(true);
        if (rowIndex === conversation.length) {
          setRowIndex(-1);
          setDisable(false);
        } else {
          setRowIndex(index);
          readText({
            text: conversation[index],
            isQuestion: index % 2 === 0,
          });
        }
      },
      rowSelected === -1 ? 0 : 1000
    );
  };

  useEffect(() => {
    let text = [];
    for (var index = 0; index < questions.length; index++) {
      if (questions[index]) text.push(questions[index]);
      if (answers[index]) text.push(answers[index]);
    }
    setConversation(text);
  }, [answers, questions]);

  useEffect(() => {
    if (rowIndex > -1) {
      if (!speaking) {
        readConversation(rowIndex + 1);
      }
    }
  }, [rowIndex, speaking]);

  const handleReadRow = (index) => {
    if (!speaking) {
      readText({
        text: conversation[index],
        isQuestion: index % 2 === 0,
      });
      setRowSelected(index);
    }
  };

  return (
    <div>
      {conversation && conversation.length > 0 && (
        <>
          <CListGroup>
            {conversation.map((item, rowKey) => {
              return (
                <CListGroupItem
                  component="a"
                  key={rowKey}
                  active={rowIndex === rowKey || rowSelected === rowKey}
                  color={
                    rowSelected === rowKey && rowKey % 2 !== 0 ? "success" : ""
                  }
                  onClick={() => handleReadRow(rowKey)}
                >
                  {item}
                </CListGroupItem>
              );
            })}
          </CListGroup>
          <div className="space-10"></div>
          <CButton
            color="warning"
            onClick={() => readConversation()}
            disabled={disable}
          >
            Conversation
          </CButton>
        </>
      )}
    </div>
  );
}

export default Conversation;
