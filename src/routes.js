import React from "react";
import AppConfig from "./AppConfig";

// ALL PAGES
const Home = React.lazy(() => import("./views/admin/home/Home"));
const Page404 = React.lazy(() => import("./views/admin/page404/Page404"));
const Logout = React.lazy(() => import("./views/admin/logout/Logout"));
const Devices = React.lazy(() => import("./views/admin/devices/Devices"));
const NewEditDevice = React.lazy(() =>
  import("./views/admin/devices/NewEditDevice")
);
const Chat = React.lazy(() => import("./views/admin/chat/Chat"));
const TypesDevice = React.lazy(() =>
  import("./views/admin/types_devices/TypesDevice")
);
const EditTypesDevice = React.lazy(() =>
  import("./views/admin/types_devices/EditTypesDevice")
);
const Areas = React.lazy(() => import("./views/admin/areas/Areas"));
const EditArea = React.lazy(() => import("./views/admin/areas/EditArea"));

const QuestionType = React.lazy(() =>
  import("./views/admin/learning/QuestionType")
);
const EditQuestionType = React.lazy(() =>
  import("./views/admin/learning/EditQuestionType")
);

const Question = React.lazy(() => import("./views/admin/learning/Question"));

const EditQuestion = React.lazy(() =>
  import("./views/admin/learning/EditQuestion")
);

const Media = React.lazy(() => import("./views/widgets/media/Media"));

const Lesson = React.lazy(() => import("./views/admin/learning/Lesson"));

const EditLesson = React.lazy(() =>
  import("./views/admin/learning/EditLesson")
);

const EnglishBooks = React.lazy(() =>
  import("./views/admin/learning/EnglishBooks")
);
const EditEnglishBook = React.lazy(() =>
  import("./views/admin/learning/EditEnglishBook")
);

const routes = [
  { path: "/admin", exact: true, name: "Home", element: Home },
  { path: "/admin/notfound", name: "Page Not Found", element: Page404 },
  { path: "/admin/logout", name: "Logout", element: Logout },
  {
    path: "/admin/devices",
    name: "Devices",
    element: AppConfig.enabledModuleDevices ? Devices : Page404,
  },
  {
    path: "/admin/new-device",
    name: "New Device",
    element: AppConfig.enabledModuleDevices ? NewEditDevice : Page404,
  },
  {
    path: "/admin/edit-device/:id",
    name: "Edit Device",
    element: AppConfig.enabledModuleDevices ? NewEditDevice : Page404,
  },
  {
    path: "/admin/chat",
    name: "Chat",
    element: AppConfig.enabledModuleChat ? Chat : Page404,
  },
  {
    path: "/admin/types-device",
    name: "Types Device",
    element: AppConfig.enabledModuleDevices ? TypesDevice : Page404,
  },
  {
    path: "/admin/new-types-device",
    name: "New Types Device",
    element: AppConfig.enabledModuleDevices ? EditTypesDevice : Page404,
  },
  {
    path: "/admin/edit-types-device/:id",
    name: "Edit Types Device",
    element: AppConfig.enabledModuleDevices ? EditTypesDevice : Page404,
  },
  {
    path: "/admin/areas",
    name: "Areas",
    element: AppConfig.enabledModuleDevices ? Areas : Page404,
  },
  {
    path: "/admin/new-area",
    name: "New Area",
    element: AppConfig.enabledModuleDevices ? EditArea : Page404,
  },
  {
    path: "/admin/edit-area/:id",
    name: "Edit Area",
    element: AppConfig.enabledModuleDevices ? EditArea : Page404,
  },
  {
    path: "/admin/question-types",
    name: "Question Types",
    element: QuestionType,
  },
  {
    path: "/admin/edit-question-type/:id",
    name: "Edit Question Type",
    element: AppConfig.enabledModuleLearning ? EditQuestionType : Page404,
  },
  {
    path: "/admin/new-question-type",
    name: "New Question Type",
    element: AppConfig.enabledModuleLearning ? EditQuestionType : Page404,
  },
  {
    path: "/admin/questions",
    name: "Question",
    element: AppConfig.enabledModuleLearning ? Question : Page404,
  },
  {
    path: "/admin/edit-question/:id",
    name: "Edit Question",
    element: AppConfig.enabledModuleLearning ? EditQuestion : Page404,
  },
  {
    path: "/admin/new-question",
    name: "New Question",
    element: AppConfig.enabledModuleLearning ? EditQuestion : Page404,
  },
  {
    path: "/admin/media",
    name: "Media",
    element: AppConfig.enabledModuleMedia ? Media : Page404,
  },
  {
    path: "/admin/lessons",
    name: "Lesson",
    element: AppConfig.enabledModuleLearning ? Lesson : Page404,
  },
  {
    path: "/admin/edit-lesson/:id",
    name: "Edit Lesson",
    element: AppConfig.enabledModuleLearning ? EditLesson : Page404,
  },
  {
    path: "/admin/new-lesson",
    name: "New Lesson",
    element: AppConfig.enabledModuleLearning ? EditLesson : Page404,
  },
  {
    path: "/admin/english-books",
    name: "English Books",
    element: AppConfig.enabledModuleLearning ? EnglishBooks : Page404,
  },
  {
    path: "/admin/edit-english-book/:id",
    name: "Edit English Book",
    element: AppConfig.enabledModuleLearning ? EditEnglishBook : Page404,
  },
  {
    path: "/admin/new-english-book",
    name: "New English Book",
    element: AppConfig.enabledModuleLearning ? EditEnglishBook : Page404,
  },
];

export default routes;
